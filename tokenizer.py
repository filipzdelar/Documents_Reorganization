import pickle
import re
import emoji
from nltk.corpus import wordnet
import nltk
from nltk.tokenize import word_tokenize
from nltk import pos_tag
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer



nltk.download('punkt')
nltk.download('stopwords')
nltk.download('averaged_perceptron_tagger')
nltk.download('wordnet')

corpus = []
corpus = pickle.load(open("corpus_eng.p", "rb"))

print(len(corpus))


def preprocess_data(text):
  #re.sub(pattern, replace, text)
  text = re.sub('@\S+', '', text) #remove @user
  text = re.sub('https?://\S+', '', text) #remove links
  text = re.sub(r'([^\w\s,])', r'\1 ', text) #add space after emoji
  text = emoji.demojize(text) #replace emoji with word
  text = re.sub('[^a-zA-Z ]', '', text) #remove punctuation marks and special chars
  text = re.sub('\s+', ' ', text) #replace multiple spaces with one
  text = text.lower().strip() #to lower case and trim
  return text


def get_wordnet_pos(word):
    """Map POS tag to first character lemmatize() accepts"""
    tag = nltk.pos_tag([word])[0][1][0].upper()
    tag_dict = {"J": wordnet.ADJ,
                "N": wordnet.NOUN,
                "V": wordnet.VERB,
                "R": wordnet.ADV}

    return tag_dict.get(tag, wordnet.NOUN)

def get_tokenized_text(text):
  final_words = []
  text = word_tokenize(text) #tokenize
  lemmatizer = WordNetLemmatizer()
  for word in text:
        if word not in stopwords.words('english'): #remove stopwords
            lemmatized_words = lemmatizer.lemmatize(word, get_wordnet_pos(word)) #lemmatize with part of speach tag
            final_words.append(lemmatized_words)
  return final_words

corpus_eng_processed = []

for text in corpus:
  preprocessed_text = preprocess_data(text)
  tokenized_text = get_tokenized_text(preprocessed_text)
  corpus_eng_processed.append(tokenized_text)

print(len(corpus_eng_processed))
pickle.dump(corpus_eng_processed, open("corpus_eng_processed.p", "wb"))