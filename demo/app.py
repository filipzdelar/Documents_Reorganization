import tkinter as tk
from tkinter import ttk
from tkinter import *
from turtle import width
from scipy.special import logsumexp

from googletrans import Translator

# root window
root = tk.Tk()
root.geometry("700x350")
root.title('Classifivation of the sentance')
T = Text(root, height = 5, width = 52)
 
# Create label
text_label = Label(root, text = "Тип класе")
text_label.config(font =("Courier", 14))
 

# Create an Exit button.
b2 = Button(root, text = "Излаз",
            command = root.destroy)
 
 
my_entry = Entry(root)
my_entry.pack(padx=20, pady=20)
# Insert The Fact.


import pickle
import gensim
from gensim.models import CoherenceModel, LdaModel, TfidfModel
#from sklearn.decomposition import NMF
from gensim.models.nmf import Nmf
import numpy as np

import pickle
import re
import emoji
from nltk.corpus import wordnet
import nltk
from nltk.tokenize import word_tokenize
from nltk import pos_tag
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer

def preprocess_data(text):
  #re.sub(pattern, replace, text)
  text = re.sub('@\S+', '', text) #remove @user
  text = re.sub('https?://\S+', '', text) #remove links
  text = re.sub(r'([^\w\s,])', r'\1 ', text) #add space after emoji
  text = emoji.demojize(text) #replace emoji with word
  text = re.sub('[^a-zA-Z ]', '', text) #remove punctuation marks and special chars
  text = re.sub('\s+', ' ', text) #replace multiple spaces with one
  text = text.lower().strip() #to lower case and trim
  return text


def get_wordnet_pos(word):
    """Map POS tag to first character lemmatize() accepts"""
    tag = nltk.pos_tag([word])[0][1][0].upper()
    tag_dict = {"J": wordnet.ADJ,
                "N": wordnet.NOUN,
                "V": wordnet.VERB,
                "R": wordnet.ADV}

    return tag_dict.get(tag, wordnet.NOUN)

def get_tokenized_text(text):
  final_words = []
  text = word_tokenize(text) #tokenize
  lemmatizer = WordNetLemmatizer()
  for word in text:
        if word not in stopwords.words('english'): #remove stopwords
            lemmatized_words = lemmatizer.lemmatize(word, get_wordnet_pos(word)) #lemmatize with part of speach tag
            final_words.append(lemmatized_words)
  return final_words

corpus_eng_processed = pickle.load(open(".\\..\corpus_eng_processed.p", "rb"))
#dictionary_s = pickle.load(open("C:\\Users\\Panonit\\Desktop\\Pravna\\Project\\Documents_Reorganization\\dictionary.p", "rb"))
dictionary = gensim.corpora.Dictionary(corpus_eng_processed)

dictionary.filter_extremes(
    no_below=2,
    no_above=0.9,
    keep_n=100000
)

corpus = [dictionary.doc2bow(text) for text in corpus_eng_processed]

tfidf = list(TfidfModel(dictionary=dictionary)[corpus])

Nmf_load = Nmf(corpus=tfidf,
          num_topics=14,
          id2word=dictionary,
          chunksize=500,
          passes=10,
          kappa=.1,
          minimum_probability=0.01,
          w_max_iter=300,
          w_stop_condition=0.0001,
          h_max_iter=100,
          h_stop_condition=0.001,
          eval_every=10,
          normalize=True,
          random_state=42
)

Nmf_load = Nmf.load(".\\..\\nmf_save") #pickle.load(open("C:\\Users\\Panonit\\Desktop\\Pravna\\Project\\Documents_Reorganization\\nmf.p", "rb"))



map_topics = {
  0 : "Javne usluge",
  1 : "Državne institucije",
  2 : "Investicije",	
  3 : "Umetnost i kultura",
  4 : "Pravne regulative",	
  5 : "Uprava",	
  6 : "Prekršaji",
  7 : "Tržište",
  8 : "Finansijske",
  9 : "Edukacione",
  10 : "Nadležnost",
  11 : "Vremenska odrednica",
  12 : "Nesvrstano",
  13 : "Pokrainske nadleženosti"
}

translator = Translator()

def get_class(text_srb):
    
    text_eng = translator.translate(text_srb, src='sr', dest='en')
    print()
    print(text_eng.text)
    print()
    preprocessed_text = preprocess_data(text_eng.text)
    tokenized_text = get_tokenized_text(preprocessed_text)
    #other_corpus = dictionary.doc2bow(tokenized_text)
    other_corpus = [dictionary.doc2bow(text) for text in [tokenized_text]]

    unseen_doc = other_corpus
    vector = Nmf_load[unseen_doc] 

    max_v = vector[0][1]
    vector = vector[0]
    for v in range(len(vector)):
      if vector[v][1] > max_v[1]:
        max_v = vector[v]

    print(max_v)
    return map_topics[max_v[0]]
    #topics_best_score = [sorted(t, key=lambda t: t[1]) for t in topics]


def print_text():
    global text_label
    if my_entry.get():
        if text_label.winfo_exists() == 1:
            text_label.destroy()
        text = my_entry.get()
        topic = get_class(text)
        text_label = Label(root, text=topic,
                           fg='black', 
                           font='none 20 bold')
        text_label.pack(padx=50, pady=50)
        

my_button = Button(root, text='Добави тип текста', 
                   fg='black', 
                   font='arial 20',
                   command=print_text)

my_button.pack(padx=20, pady=20)
b2.pack(padx=20, pady=20)
text_label.pack(padx=20, pady=20)


root.mainloop()