Član 1

Ovom Odlukom ustanovljavaju se priznanja, nagrade i povelje grada Smedereva, kao i nagrade festivala grada Smedereva koji su pod pokroviteljstvom grada Smedereva i uređuje postupak, nadležnosti i uslovi za njihovu dodelu.

I PRIZNANJA GRADA SMEDEREVA

Član 2

Priznanja grada Smedereva su:

- "Ključ grada Smedereva"

- "Svetosavska povelja"

- "Srebrnjak grada Smedereva" i

- "Zlatni grozd smederevke"

Član 3

Priznanja se dodeljuju fizičkim i pravnim licima za izuzetne rezultate u oblasti privrede, vaspitanja i obrazovanja, kulture, umetnosti, nauke, fizičke kulture, zdravstvene, socijalne i dečije zaštite, informisanja, lokalne samouprave i svim drugim vidovima stvaralaštva u gradu Smederevu.

Priznanje se može dodeliti i istaknutim fizičkim ili pravnim licima iz drugih sredina za doprinos razvoju grada Smedereva i zasluge na unapređenju saradnje sa Gradom.

Član 4

Jedno priznanje se može dodeliti istom fizičkom ili pravnom licu samo jedanput.

1. "KLJUČ GRADA SMEDEREVA"

Član 5

"Ključ grada Smedereva" dodeljuje se domaćim i stranim institucijama, opštinama, gradovima i drugim fizičkim i pravnim licima za izuzetne zasluge na uspostavljanju saradnje u svim oblastima privrede i društvene nadgradnje i ostvarivanje projekata od posebnog značaja za grad Smederevo.

Član 6

"Ključ grada Smedereva" je srednjevekovni smederevski ključ izliven od mesinga, dužine 255 mm. Glava ključa je elipsasta rozeta, koju čini osmokraka zvezda sa laticama. Telo ključa je obličasto sa četvrtastim delom do glave, dimenzija 36 x 16 mm, čije su bočne pravougaone strane po visini ukrašene ornamentom u vidu riblje kosti, s tim što su na dužim ivicama četiri zaseka, a na prednjoj strani udubljenje. Pero ključa je pravougaonog oblika, nesimetrično prorezano prema telu, a sa prednje strane su dva ureza i dva zuba.

"Ključ grada Smedereva" se dodeljuje u kožnoj kutiji, dimenzija 330 x 150 mm, tako da se sa unutrašnje strane kutije nalazi mesingana pločica sa urezanim imenom i prezimenom, odnosno nazivom dobitnika i nazivom oblasti za koju se priznanje dodeljuje.

Uz "Ključ grada Smedereva" dodeljuje se i akt na kvalitetnom papiru formata A3, na kome je ćirilicom ispisan tekst:

GRAD SMEDEREVO
dodeljuje
"KLJUČ GRADA SMEDEREVA"
________________________
za
________________________

Smederevo,
____________ godine

GRADONAČELNIK

Član 7

Inicijativu za dodelu priznanja "Ključ grada Smedereva" mogu dati fizička i pravna lica.

Inicijativa se podnosi u pisanoj formi, mora biti obrazložena i mora sadržati podatke o rezultatima rada fizičkog, odnosno pravnog lica koje se predlaže i potpisana od strane predlagača.

Član 8

Predlog za dodelu priznanja "Ključ grada Smedereva" utvrđuje Komisija za kadrovska pitanja i javna priznanja.

U postupku razmatranja inicijative i utvrđivanja predloga za dodelu priznanja "Ključ grada Smedereva", Komisija može angažovati stručne organizacije ili udruženja iz odgovarajućih oblasti, radi konsultovanja ili pribavljanja mišljenja.

Po razmatranju datih inicijativa Komisija utvrđuje predlog i dostavlja ga gradonačelniku grada Smedereva na odlučivanje.

"Ključ grada Smedereva" dodeljuje gradonačelnik grada Smedereva.

2. "SVETOSAVSKA POVELJA"

Član 9

"Svetosavska povelja" dodeljuje se fizičkim i pravnim licima za izuzetne rezultate ostvarene u dužem vremenskom periodu u oblasti prosvete, nauke, kulture i zdravstva.

Član 10

"Svetosavska povelja" izrađuje se na pergament papiru, pravougaonog oblika, dimenzija 330 x 500 mm i sadrži tekst ispisan ćirilicom:

GRAD SMEDEREVO
dodeljuje
"SVETOSAVSKU POVELJU"
________________________
za višegodišnje izuzetne rezultate i doprinos razvoju
prosvete, nauke, kulture i zdravstva, a posebno za oblast
_______________________________

Smederevo,
_____________ godine

GRADONAČELNIK

Tekst je u ornament okviru, a po sredini gore je utisnut znak sa likom Svetog Save, u kombinaciji crvene, plave i bele boje.

Član 11

Organizacioni odbor Svetosavskih svečanosti raspisuje konkurs za izbor kandidata za dobijanje "Svetosavske povelje" i dostavlja pristigle predloge kandidata Komisiji za kadrovska pitanja i javna priznanja.

Predlog kandidata Komisija dostavlja gradonačelniku na odlučivanje.

"Svetosavsku povelju" dodeljuje gradonačelnik grada Smedereva.

"Svetosavska povelja" se u toku kalendarske godine može dodeliti samo jedna, a u izuzetnim slučajevima dve.

3. "SREBRNJAK GRADA SMEDEREVA"

Član 12

"Srebrnjak grada Smedereva" dodeljuje se fizičkim i pravnim licima, koja su se naročito istakla postignutim rezultatima u svim oblastima društvenog života.

Član 13

"Srebrnjak grada Smedereva" je uvećan revers novca despota Đurđa Brankovića iz XV veka, kružnog oblika prečnika 55 mm, debljine 5 mm, sa stilizovanom figurom lava u pokretu, izrađen od odgovarajućih legura i posrebren.

Ime Smedereva ispisano je na staroslovenskom jeziku i to s leva na desno u krug počev od glave lava.

"Srebrnjak grada Smedereva" dodeljuje se u kožnoj kutiji dimenzija 110 x 110 mm, tako da se sa unutrašnje strane kutije nalazi mesingana pločica sa urezanim imenom i prezimenom, odnosno nazivom dobitnika i nazivom oblasti za koju se priznanje dodeljuje.

Član 14

O dodeli "Srebrnjaka grada Smedereva" odlučuje gradonačelnik grada Smedereva.

4. "ZLATNI GROZD SMEDEREVKE"

Član 15

"Zlatni grozd smederevke" se dodeljuje fizičkim i pravnim licima za izuzetne rezultate u poljoprivredi (vinogradarstva, voćarstva, vinarstva i drugo), privredno-turističkim manifestacijama, a može se dodeliti i ugostiteljskim objektima i drugim subjektima za višegodišnju uspešnu poslovnu saradnju.

Član 16

"Zlatni grozd smederevke" predstavlja stilizovani grozd sorte smederevke, kružnog oblika prečnika 150 mm i debljine 5 mm, izrađen od odgovarajućih legura.

"Zlatni grozd smederevke" se dodeljuje u kožnoj kutiji, dimenzija 215 x 210 mm, tako da se sa unutrašnje strane kutije nalazi mesingana pločica sa urezanim imenom i prezimenom, odnosno nazivom dobitnika i nazivom oblasti za koju se priznanje dodeljuje.

Član 17

O dodeli "Zlatnog grozda smederevke" odlučuje gradonačelnik grada Smedereva na predlog Organizacionog odbora Privredno-turističke manifestacije "Smederevska jesen".

Priznanje se dodeljuje u vreme održavanja Privredno-turističke manifestacije "Smederevska jesen".

II NAGRADE GRADA SMEDEREVA

Član 18

Nagrade grada Smedereva su:

- "Nagrada despot Đurađ" i

- "Svetosavska nagrada za stvaralaštvo mladih".

1. "NAGRADA DESPOT ĐURAĐ"

Član 19

"Nagrada despot Đurađ" dodeljuje se fizičkim i pravnim licima, za izuzetne rezultate ostvarene u svim privrednim i društvenim oblastima koji doprinose razvoju grada Smedereva.

Član 20

Nagrada se sastoji od poprsja despota Đurđa Brankovića, izrađenog od bronze, na mermernom postolju i novčanog dela u iznosu jedne prosečne mesečne neto zarade ostvarene u privredi grada Smedereva, u poslednja tri meseca, a prema poslednjim poznatim podacima Republičkog zavoda za statistiku.

Na postolju poprsja despota Đurđa Brankovića nalazi se mesingana pločica sa urezanim imenom i prezimenom, odnosno nazivom dobitnika i nazivom oblasti za koju se nagrada dodeljuje.

Uz "Nagradu despot Đurađ" dodeljuje se i akt na kvalitetnom papiru formata A3 na kome je ćirilicom ispisan tekst:

GRAD SMEDEREVO
dodeljuje
"NAGRADU DESPOT ĐURAĐ"
________________________
za
____________________________________

Smederevo,
____________ godine

GRADONAČELNIK

Član 21

"Nagrada despot Đurađ" može se dodeliti istom fizičkom ili pravnom licu samo jedanput, a novčani deo nagrade dodeljuje se samo fizičkom licu.

Član 22

Inicijativu za dodelu "Nagrade despot Đurađ" mogu dati fizička i pravna lica, na konkurs koji raspisuje predsednik Skupštine grada Smedereva.

Inicijativa se podnosi u pisanoj formi, mora biti obrazložena, mora sadržati podatke o rezultatima rada fizičkog, odnosno pravnog lica koje se predlaže i potpisana od strane predlagača.

Član 23

Predlog za dodelu "Nagrade despot Đurađ" utvrđuje Komisija za kadrovska pitanja i javna priznanja.

U postupku razmatranja inicijative i utvrđivanja predloga za dodelu "Nagrade despot Đurađ", Komisija može angažovati stručne organizacije ili udruženja iz odgovarajućih oblasti radi konsultovanja ili pribavljanja mišljenja.

Po razmatranju datih inicijativa Komisija utvrđuje predlog i dostavlja ga gradonačelniku grada Smedereva na odlučivanje.

"Nagradu despot Đurađ" dodeljuje gradonačelnik grada Smedereva povodom Dana grada Smedereva.

2. "SVETOSAVSKA NAGRADA ZA STVARALAŠTVO MLADIH"

Član 24

"Svetosavska nagrada za stvaralaštvo mladih" dodeljuje se za najuspešnije rezultate i ostvarenja učenika i studenata, koji imaju prebivalište na teritoriji grada Smedereva, i to:

- učenicima osnovnih i srednjih škola, koji imaju prosečnu ocenu 5.0, koji su u toku školovanja u osnovnoj i srednjoj školi osvojili najmanje jedno od prva tri mesta na republičkom ili međunarodnom takmičenju u pojedinačnoj konkurenciji iz opšte obrazovanih ili stručnih predmeta i područja rada, u organizaciji Ministarstva prosvete, nauke i tehnološkog razvoja, odgovarajućih stručnih društava prosvetnih radnika ili zajednica srednjih stručnih i umetničkih škola;

- studentima fakulteta i akademija, koji su i osnovne studije završili u roku sa najnižom prosečnom ocenom 9.60.

Nagrada se dodeljuje za rezultate u prethodnoj školskoj godini.

Član 25

"Svetosavska nagrada za stvaralaštvo mladih" sastoji se iz novčanog dela u iznosu jedne prosečne mesečne neto zarade ostvarene u privredi grada Smedereva, u poslednja tri meseca, a prema poslednjim poznatim podacima Republičkog zavoda za statistiku.

Uz nagradu se dodeljuje i akt na kvalitetnom papiru formata A4, na kome je ćirilicom ispisan tekst:

GRAD SMEDEREVO
dodeljuje
"SVETOSAVSKU NAGRADU ZA STVARALAŠTVO MLADIH"
_______________________
za
_____________________________________

Smederevo,
__________________ godine

GRADONAČELNIK

Član 26

Organizacioni odbor Svetosavskih svečanosti raspisuje konkurs za izbor kandidata za dobijanje "Svetosavske nagrade za stvaralaštvo mladih" i dostavlja pristigle predloge kandidata Komisiji za kadrovska pitanja i javna priznanja.

Predlog kandidata Komisija dostavlja gradonačelniku na odlučivanje.

"Svetosavsku nagradu za stvaralaštvo mladih" dodeljuje gradonačelnik grada Smedereva na dan Svetog Save.

III POVELJE GRADA SMEDEREVA

Član 27

Povelje grada Smedereva su:

- "Povelja grada Smedereva" i

- "Povelja počasnog građanina grada Smedereva".

1. "POVELJA GRADA SMEDEREVA"

Član 28

"Povelja grada Smedereva" dodeljuje se fizičkim i pravnim licima za ostvarene izvanredne rezultate i dostignuća u radu u oblasti privrede, kulture, umetnosti, nauke, medicine, arhitekture i urbanizma, novinarstva, obrazovanja, sporta, doprinosa u akcijama socijalnog i humanitarnog karaktera, stvaralaštva mladih i drugim oblastima.

Pod ostvarenim izvanrednim rezultatima i dostignućima koji su postali dostupni javnosti podrazumeva se:

1) u oblasti privrede:

- rad ili rezultati izuzetne vrednosti fizičkog ili pravnog lica kojima je dat značajan doprinos afirmaciji, razvoju i unapređenju privrede;

2) u oblasti kulture i umetnosti:

- za postignute rezultate i stvaralački doprinos i širenju kulture,

- za doprinos na širenju književnog i prevodilačkog stvaralaštva,

- izvedeno, odnosno prikazano delo na pozorišnoj sceni i drugom prostoru namenjenom za izvođenje dela ove vrste,

- prikazano filmsko i radio-televizijsko ostvarenje u bioskopskoj dvorani, ili emitovano u radio-televizijskom programu i drugim oblicima elektronskih i pisanih medija,

- izloženo, odnosno prezentirano delo u galeriji i drugom prostoru namenjenom za prezentaciju dela likovnog i primenjenog stvaralaštva, vizuelnih i proširenih medija,

- izvedeno muzičko ili muzičko-scensko delo u koncertnoj ili pozorišnoj dvorani i drugom prostoru namenjenom za izvođenje dela ove vrste;

3) u oblasti nauke:

- naučno ostvarenje, odnosno rad u društvenim, humanističkim, prirodnim i tehničkim naukama, koji kao rezultat ima nova saznanja i sintezu postojećih u cilju njihove primene, koji predstavlja doprinos razvoju naučne oblasti ili razvoju novih proizvoda, metoda ili tehnoloških postupaka, pod uslovom da je objavljeno u naučnoj i stručnoj literaturi, odgovarajućim naučnim časopisima ili drugim načinom javnog saopštavanja,

- pronalazak koji je u smislu odredaba Zakona o patentima, priznat i upisan u Zakonom utvrđen registar;

4) u oblasti medicine:

- rad ili rezultati izuzetne vrednosti fizičkog ili pravnog lica kojima je dat značajan doprinos razvoju i unapređenju medicine i zdravstvene zaštite ljudi;

5) u oblasti arhitekture i urbanizma:

- realizovano delo iz oblasti arhitekture, urbanizma ili rekonstrukcije objekata,

- urbanistički planovi za koncepciju i metodologiju pod uslovom da su usvojeni i da je njihova realizacija otpočela;

6) u oblasti novinarstva:

- vrhunsko ostvarenje fizičkog ili pravnog lica u novinarstvu ili publicistici, za seriju ili više napisa, televizijskih ili radijskih priloga ili emisija, publicističkih radova o temama od značaja za ukupan društveni život i drugim oblicima elektronskih i pisanih medija,

- kreativni doprinos uređivanju rubrike, emisije i programa kojima se značajno doprinosi informisanju građana;

7) u oblasti obrazovanja:

- izuzetno delo i rezultat vaspitno-obrazovnog ili instruktivnog rada i rezultat u organizaciji i razvoju školstva u celini, ili u pojedinim njegovim oblastima;

8) u oblasti sporta:

- rad ili rezultati izuzetne vrednosti fizičkog ili pravnog lica, kojima je dat značajan doprinos afirmaciji, razvoju i unapređenju sporta (vrhunski sportisti-članovi sportskih organizacija, odnosno samostalni profesionalni sportisti, za izvanredne rezultate u određenoj grani sporta; sportski stručnjaci, stručnjaci u sportu i naučni radnici za naročito vredne doprinose unapređenju oblasti sporta; sportski radnici za doprinos organizovanju, razvoju i unapređenju oblasti sporta);

9) u oblasti socijalnog i humanitarnog rada:

- rad ili rezultati izuzetne vrednosti u oblasti socijalnog i humanitarnog rada.

Član 29

"Povelja grada Smedereva" sadrži prepoznatljive simbole grada Smedereva kao likovno-grafičko rešenje i tekst ispisan ćirilicom:

GRAD SMEDEREVO
dodeljuje
"POVELJU GRADA SMEDEREVA"
__________________________
za
__________________________________

Smederevo,
________________ godine

GRADONAČELNIK

Član 30

Organizacioni odbor za dodelu "Povelje grada Smedereva" predlaže kandidata za dobijanje Povelje gradonačelniku grada Smedereva, koji ovu Povelju dodeljuje na dan Grada.

2. "POVELJA POČASNOG GRAĐANINA GRADA SMEDEREVA"

Član 31

"Povelja počasnog građanina grada Smedereva" se dodeljuje fizičkim licima za izuzetne rezultate kojima su doprineli razvoju i unapređenju grada Smedereva.

Član 32

"Povelja počasnog građanina grada Smedereva" sadrži prepoznatljive simbole grada Smedereva, kao likovno-grafičko rešenje, i tekst ispisan ćirilicom na kvalitetnom papiru A3 formata, tehnikom zlatotiska:

GRAD SMEDEREVO
dodeljuje
"POVELJU POČASNOG GRAĐANINA GRADA SMEDEREVA"
__________________________
za
__________________________________________

Smederevo,
________________ godine

PREDSEDNIK
SKUPŠTINE GRADA SMEDEREVA

Član 33

Predloge kandidata za dodelu "Povelje počasnog građanina grada Smedereva" mogu dostaviti Komisija za kadrovska pitanja i javna priznanja i Gradsko veće grada Smedereva.

Predlozi iz stava 1. ovog člana dostavljaju se Skupštini grada Smedereva na odlučivanje.

IV MANIFESTACIJE U OBLASTI KULTURE KOJE SU POD POKROVITELJSTVOM GRADA SMEDEREVA

Član 34

Manifestacije u oblasti kulture koje su pod pokroviteljstvom grada Smedereva su:

- Međunarodni festival poezije "Smederevska pesnička jesen",

- Festival "Nušićevi dani" i

- Festival "Tvrđava teatar Smederevo"

u okviru kojih se dodeljuju posebne nagrade.

1. "ZLATNI KLJUČ SMEDEREVA"

Član 35

"Zlatni ključ Smedereva" je nagrada koja se dodeljuje uglednim domaćim i stranim pesnicima za pesnički opus, a dodeljuje se u okviru Međunarodnog festivala poezije Smederevska pesnička jesen.

"Zlatni ključ Smedereva" je nagrada koja se istom licu može dodeliti samo jedanput.

Član 36

"Zlatni ključ Smedereva" je nagrada koja se sastoji od pozlaćene kopije ključa srednjevekovnog Smedereva, objavljivanja knjige dvojezičnog izdanja - izbora poezije i novčanog dela u iznosu od 1000 eura u dinarskoj protivvrednosti, po srednjem kursu Narodne banke Srbije na dan uručenja.

"Zlatni ključ Smedereva" se dodeljuje u kožnoj kutiji dimenzija 330 x 150 mm, tako da se sa unutrašnje strane kutije nalazi mesingana pločica sa urezanim imenom i prezimenom pesnika koji je ovu nagradu dobio.

Član 37

O dobitniku "Zlatnog ključa Smedereva" odlučuje Odbor za dodelu nagrada koji formira Organizacioni odbor Smederevske pesničke jeseni.

Nagradu uručuje gradonačelnik grada Smedereva u okviru programa Festivala.

2. "NUŠIĆEVA NAGRADA" ZA ŽIVOTNO DELO GLUMCU KOMIČARU

Član 38

"Nušićeva nagrada" za životno delo glumcu komičaru se dodeljuje kao društveno priznanje za izuzetan doprinos u pozorišnoj umetnosti i u cilju negovanja lika i dela najznačajnijeg srpskog komediografa Branislava Nušića.

Član 39

"Nušićeva nagrada" za životno delo glumcu komičaru dodeljuje se u vidu skulpture Branislava Nušića.

Nagrada je slojevita i predstavlja statuetu Branislava Nušića izvedenu u trajnom materijalu - bronzi, visine 350 mm, sa graviranom pločicom, apliciranom na postolju statuete.

Pločica sadrži: naziv festivala, ime dobitnika, odgovarajući kraći tekst i u donjem delu po sredini naziv grada, datum i godinu uručenja.

Dobitniku nagrade se pored skulpture dodeljuje i novčani iznos od 1000 eura u dinarskoj protivvrednosti, po srednjem kursu Narodne banke Srbije na dan uručenja.

Tekst natpisa pločice na postolju statuete ispisan je ćiriličnim pismom i glasi:

CENTAR ZA KULTURU SMEDEREVO
POZORIŠNI FESTIVAL
(redni broj)
"NUŠIĆEVI DANI"

"NUŠIĆEVA NAGRADA"
za životno delo glumcu komičaru

(ime i prezime glumca)
GRAD SMEDEREVO

Član 40

"Nušićeva nagrada" za životno delo glumcu komičaru se dodeljuje na osnovu odluke žirija, a uručuje je gradonačelnik grada Smedereva na završnoj večeri Festivala "Nušićevi dani".

3. NAGRADA "TVRĐAVA TEATAR SMEDEREVO" ZA NAJBOLJU PREDSTAVU

Član 41

Nagrada "Tvrđava teatar Smederevo" za najbolju predstavu se dodeljuje u okviru Međunarodnog ambijentalnog pozorišnog festivala "Tvrđava teatar Smederevo".

Član 42

Nagrada "Tvrđava teatar Smederevo" za najbolju predstavu dodeljuje se u vidu bronzane statuete koja predstavlja grb despota Đurđa Brankovića.

Na postolju se nalazi mesingana pločica, dimenzije 100 x 25 mm, sa natpisom sledeće sadržine:

NAGRADA "TVRĐAVA TEATAR SMEDEREVO"
ZA NAJBOLJU PREDSTAVU

(Naziv predstave)

(Naziv pozorišta)

(Ime reditelja)

Smederevo
(Datum)

Član 43

Nagrada "Tvrđava teatar Smederevo" za najbolju predstavu dodeljuje se na osnovu odluke tročlanog stručnog žirija, a uručuje je gradonačelnik grada Smedereva na završnoj večeri festivala "Tvrđava teatar Smederevo".

Član 44

Zadužuje se Kabinet gradonačelnika grada Smedereva da vodi evidenciju o dobitnicima svih nagrada, priznanja i povelja, i iste čuva u sefu, tako što će na numerisanim stranama koje će biti zaštićene voskom detaljno ispisivati: naziv nagrade, ime i prezime dobitnika i godinu dodele.

Član 45

Odluka o dodeli priznanja, nagrada i povelja grada Smedereva sadrži ime dobitnika, naziv priznanja, odnosno nagrade ili povelje i kratko obrazloženje za njihovu dodelu.

Član 46

Sredstva za priznanja, nagrade i povelje iz ove Odluke obezbeđuju se u budžetu grada Smedereva.

Član 47

Danom stupanja na snagu ove Odluke prestaje da važi Odluka o priznanjima i nagradama opštine Smederevo ("Službeni list opštine Smederevo", broj 11/2004).

Član 48

Ova Odluka stupa na snagu osmog dana od dana objavljivanja u "Službenom listu grada Smedereva".