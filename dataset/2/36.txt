Član 1

Ovom Odlukom ustanovljavaju se Praznici grada, nagrade, javna priznanja i zvanje počasnog građanina grada Zrenjanina.

Član 2

Praznici grada Zrenjanina su:

- 28. avgust, dan Uspenja presvete Bogorodice, Velika Gospojina, Dan grba grada Zrenjanina;

- 2. oktobar, Dan oslobođenja grada Zrenjanina u Drugom Svetskom ratu;

- 31. oktobar, Dan osnivanja Srpskog narodnog odbora;

- 17. novembar, Dan oslobođenja grada Zrenjanina u Prvom Svetskom ratu.

Član 3

Dan 28. avgust obeležava se održavanjem svečane akademije.

Dan 2. oktobar obeležava se održavanjem svečane akademije i polaganjem venaca na spomen obeležja posvećenih učesnicima u Drugom svetskom ratu.

Dan 31. oktobar obeležava se održavanjem svečane akademije i dodelom povelje "dr Slavko Županski" fizičkim licima, udruženjima i društvima koji su svojim naučnim, stručnim, kulturnim i literarnim radovima i delima doprineli očuvanju istorijskih, kulturnih i humanitarnih tradicija i uspomena grada Zrenjanina.

Dan 17. novembar koji se proslavlja kao praznik grada Zrenjanina obeležava se održavanjem svečane akademije i polaganjem venaca na spomen obeležja posvećenih učesnicima u Prvom svetskom ratu.

Član 4

"Nagrada grada Zrenjanina" dodeljuje se pojedincima i pravnim licima.

Dobitniku "Nagrade grada Zrenjanina" dodeljuje se priznanje u vidu povelje i novčana nagrada, u skladu sa predviđenim sredstvima u Odluci o budžetu grada Zrenjanina.

Član 5

"Nagrada grada Zrenjanina" dodeljuje se za postignute vidne rezultate u oblasti privrede, nauke, kulture, vaspitanja i obrazovanja, zdravstvene i socijalne zaštite, fizičke kulture, umetnosti i u drugim oblastima od interesa za grad Zrenjanin.

Član 6

"Nagrada grada Zrenjanina" ne može biti dodeljena pojedincu odnosno pravnom licu po istom osnovu dva puta.

Izuzetno, "Nagrada grada Zrenjanina" može biti dodeljena licima iz člana 1. ovog člana dva puta po istom osnovu ukoliko u proteklih 10 godina nisu bili dobitnici "Nagrade grada Zrenjanina".

Član 7

O dodeli "Nagrade grada Zrenjanina" odlučuje Komisija koju imenuje Skupština grada Zrenjanina posebnim rešenjem na period od četiri godine.

Komisija ima predsednika i četiri člana.

Jedno lice može biti imenovano za člana Komisije najviše dva puta uzastopno.

Član 8

Predloge za dodelu "Nagrade grada Zrenjanina" mogu podneti: pojedinci, Skupština grada Zrenjanina, Gradsko veće grada Zrenjanina, preduzeća, ustanove, udruženja, organizacije i druge asocijacije.

Predlog iz stava 1. ovog člana sa potrebnom dokumentacijom i obrazloženjem podnosi se Komisiji.

Član 9

Komisija donosi odluke na sednicama, većinom od ukupnog broja članova Komisije.

U slučaju odsutnosti ili sprečenosti da obavlja svoju funkciju, predsednika Komisije zamenjuje i radom sednice rukovodi član koga odredi Komisija.

O radu Komisije vodi se zapisnik koji potpisuje predsednik Komisije i zapisničar.

Svaka odluka Komisije mora biti obrazložena.

Član 10

Povelju "Nagrade grada Zrenjanina" potpisuju i na svečani način uručuju Gradonačelnik i predsednik Skupštine grada Zrenjanina.

Član 11

Skupština grada Zrenjanina može istaknutim ličnostima, za posebne zasluge za razvoj grada Zrenjanina, da dodeli zvanje "Počasnog građanina grada Zrenjanina".

Član 12

Zvanje "Počasnog građanina grada Zrenjanina" dodeljuje se za istaknuto i trajno životno delo u oblasti nauke, privrede, umetnosti, obrazovanja i vaspitanja, kao i za rezultate postignute u javnom životu i razvoju kulturnog stvaralaštva i to fizičkim licima koja su svojim delima i činjenima doprinela podizanju ugleda grada Zrenjanina u zemlji i inostranstvu.

Član 13

Stručne i administrativno-tehničke poslove za potrebe Komisije vrši nadležna organizaciona jedinica Gradske uprave grada Zrenjanina.

Član 14

Stupanjem na snagu ove Odluke prestaje da važi Odluka o ustanovljenju dana opštine, nagrade grada i drugih praznika i manifestacija opštine Zrenjanin ("Službeni list opštine Zrenjanin", br. 7/94, 5/96, 11/99 i 10/03).

Član 15

Ova odluka stupa na snagu osmog dana od dana objavljivanja u "Službenom listu grada Zrenjanina".

 

Samostalni član Odluka o izmenama i dopunama
Odluke o ustanovljenju praznika grada, nagrada, javnih priznanja i zvanja počasnog građanina grada Zrenjanina

("Sl. list Grada Zrenjanina", br. 8/2019)

Član 3

Ova odluka stupa na snagu danom objavljivanja u "Službenom listu grada Zrenjanina".