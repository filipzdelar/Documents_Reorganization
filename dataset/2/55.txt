I Imenuje se Komisija za dodelu javnih priznanja Skupštine grada Loznice u sastavu:

1. Vidoje Petrović, gradonačelnik Grada Loznice, za predsednika

2. Petar Ekmeščić, zamenik gradonačelnika, za zamenika predsednika

3. Ljubinko Đokić, dipl. ing. mašinstva, za člana

4. Milomir Savić, maš. tehničar, za člana

5. Dragan Bošković, električar, za člana

6. Pantelija Gačić, preduzetnik, za člana

7. Svetlana Zarić, dipl. ecc., za člana

8. Milan Gavrilović, građ. tehničar, za člana

9. Zoran Mitrić, preduzetnik, za člana.

II Zadatak Komisije je da razmatra prispele predloge i na osnovu kriterijuma iz člana 10. Odluke o javnim priznanjima donese Odluku o dodeli javnih priznanja i odluči o visini novčane nagrade za dobitnike Povelje.

III Rešenje objaviti u "Službenom listu Grada Loznice".