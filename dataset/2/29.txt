I OPŠTE ODREDBE

Član 1

Ovom Odlukom propisuju se uslovi, kriterijumi, postupak i organ koji donosi odluku o dodeli nagrade "Sveti Sava", kao i način uručenja iste.

Član 2

Nagrada "Sveti Sava" dodeljuje se svake godine:

1. obrazovno-vaspitnoj ustanovi ili drugoj ustanovi, koja pored svoje osnovne delatnosti participira i delatnost obrazovanja i vaspitanja;

2. vaspitaču ili stručnom saradniku koji se bavi vaspitnim-obrazovnim radom postižući pri tom izuzetne rezultate od značaja za vaspitanje i razvoj dece predškolskog uzrasta;

3. profesoru razredne nastave koji se bavi obrazovno-vaspitnim radom i postiže najbolje rezultate u radu sa decom osnovnoškolskog uzrasta do 4. razreda i na takmičenjima koja se nalaze u kalendaru Ministarstva prosvete, kao i međunarodna takmičenja koja su podržana od strane Ministarstva prosvete i Vlade Republike Srbije;

4. veroučitelju-katiheti koji se bavi obrazovno - vaspitnim, prosvetiteljskim i misionarskim radom, postižući pri tom izuzetne rezultate u radu sa učenicima;

5. nastavniku predmetne nastave osnovne škole koji se bavi obrazovno-vaspitnim radom postižući pri tom izuzetne rezultate od značaja za vaspitanje i obrazovanje učenika i na takmičenjima koja se nalaze u kalendaru Ministarstva prosvete, kao i međunarodna takmičenja koja su podržana od strane Ministarstva prosvete i Vlade Republike Srbije ;

6. profesoru predmetne nastave srednje škole koji se bavi obrazovno-vaspitnim radom i postiže izuzetne rezultate od značaja za vaspitanje i obrazovanje učenika i na takmičenjima koja se nalaze u kalendaru Ministarstva prosvete, kao i međunarodna takmičenja koja su podržana od strane Ministarstva prosvete i Vlade Republike Srbije.

7. učeniku/ci osnovne škole za postignute izuzetne rezultate u školovanju i na takmičenjima koja se nalaze u kalendaru Ministarstva prosvete, kao i međunarodna takmičenja koja su podržana od strane Ministarstva prosvete i Vlade Republike Srbije;

8. učeniku/ci srednje škole za postignute izuzetne rezultate u školovanju i na takmičenjima koja se nalaze u kalendaru Ministarstva prosvete, kao i međunarodna takmičenja koja su podržana od strane Ministarstva prosvete i Vlade Republike Srbije;

9. studentu prvog stepena osnovnih akademskih ili osnovnih strukovnih studija fakulteta ili visoke škole za izuzetan uspeh na studijama i rezultate i doprinos u oblasti naučnog rada.

Član 3

Nagrada "Sveti Sava" se sastoji od novčanog iznosa, plakete sa likom Svetog Save i statue sa likom Svetog Save. Plaketu potpisuje predsednik Organizacionog odbora manifestacije "Svetosavska nedelja".

Odluku o visini nagrade donosi Odbor za svaku godinu.

Član 4

Sredstva za isplatu nagrade "Sveti Sava" obezbeđuju se u budžetu grada Vranja.

II USLOVI ZA DODELJIVANJE NAGRADE "SVETI SAVA"

Član 5

Da bi USTANOVA iz člana 2. ove odluke mogla dobiti nagradu "Sveti Sava", mora da ispunjava sledeće uslove:

• da je uključena u međunarodne obrazovne projekte,

- da podstiče i uključuje radnike u različite oblike stručnog usavršavanja,

- da plasman učenika koje ustanova priprema ima zapažene rezultate na gradskim, okružnim, republičkim i međunarodnim takmičenjima,

- da se bavi istraživačkim projektima u vezi unapređivanja obrazovne prakse,

- da je uključena u oglede i aktivno učestvuje na stručnim skupovima, odnosno savetovanjima na republičkom nivou,

- da učestvuje u organizovanju regionalnih, odnosno republičkih seminara,

- da organizuje predavanja, tribine, smotre, književne susrete, akademije i izložbe radova na nivou grada, okruga, odnosno države,

- da promoviše i podstiče savremena dostignuća u obrazovno-vaspitnoj praksi,

- da objavljuje svoje radove u stručnim časopisima,

- da uvažava individualne karakteristike i potrebe razvojnog nivoa svakog pojedinog učenika u procesu nastave, odnosno učenja,

- da ispituje potrebe roditelja i društvene zajednice i u skladu sa tim planira saradnju sa njima,

- da posebnu pažnju poklanja osmišljavanju i uređenju prostora u samoj ustanovi i u dvorištu i kao takva služi za primer ostalima u društvenoj zajednici,

- da sarađuje sa društvenom sredinom,

- da organizuje manifestacije na nivou grada (prisutna je u javnom životu grada),

- da organizuje akcije solidarnosti, pomoći onima kojima je pomoć potrebna,

- da razvija inkluzivnu praksu,

- da podstiče učeničku i studentsku populaciju u osmišljavanju i realizaciji kulturnih sadržaja,

- da radi na razvoju ekološke svesti.

Da bi ustanova dobila nagradu "Sveti Sava" ne mora kumulativno da ispunjava sve napred navedene uslove. Nagradu će dobiti ona ustanova koja u svom dosadašnjem organizatorskom i stvaralačkom radu ima najviše uslova predviđenih članom 5. ove odluke.

Član 6

Da bi VASPITAČ ili STRUČNI SARADNIK mogao da dobije nagradu "Sveti Sava" treba da ispunjava sledeće uslove:

- da je u radnom odnosu u godini za koju se dodeljuje nagrada,

- da se ističe izuzetnim zalaganjem i značajnim postignućima u radu sa decom,

- da osmišljava podsticajnu sredinu za učenje i razvoj, usklađuje ciljeve, sadržaje i metode rada, timski planira i programira vaspitno-obrazovni rad uvažavajući individualne potrebe dece, kao i potrebe roditelja i društvene zajednice,

- primenjuje raznovrsne metode, aktivnosti i oblike rada, individualizuje rad sa decom i podstiče motivaciju za učenje, predstavlja pozitivan model deci;

- primenjuje principe nenasilne komunikacije u izražavanju svojih zapažanja u vezi sa praćenjem napredovanja dece,

- zastupa najbolji interes dece i gradi atmosferu međusobnog poverenja sa svom decom u vaspitno-obrazovnom radu,

- sarađuje sa roditeljima i društvenom zajednicom i prihvata njihove inicijative u ostvarivanju zajedničkih interesa, koristi mogućnosti društvene zajednice za podsticanje razvoja dece,

- prikazuje novine stručnom organu ustanove iz oblasti vaspitno-obrazovnog rada, izvodi ugledne aktivnosti, organizuje predavanja, tribine, smotre, susrete sa književnikom, izložbe u ustanovi odnosno društvenoj sredini; izrađuje didaktička sredstva, objavljuje stručne radove u stručnim časopisima; angažuje se u radu stručnog aktiva, odnosno strukovnog udruženja na nivou grada i šire; učestvuje na stručnim skupovima odnosno studijskim putovanjima,

- doprinosi unapređivanju kvaliteta rada ustanove i razvijanju pozitivne atmosfere u ustanovi,

- učestvuje u istraživačkom projektu u vezi unapređivanja vaspitno-obrazovne prakse. Koordinira ili vodi program ogleda

- deca sa kojom radi osvajaju nagrade na konkursima i takmičenjima;

- vodi radionicu na stručnom skupu domaćeg ili međunarodnog karaktera. Autor je istraživačkog projekta. Autor je ili koautor udžbenika ili priručnika koji su odobreni rešenjem Ministarstva prosvete, nauke i tehnološkog razvoja.

Da bi pojedinac dobio nagradu "Sveti Sava", ne mora kumulativno da ispunjava sve napred navedene uslove. Nagradu će dobiti onaj vaspitač koji u svom dosadašnjem stvaralačkom i vaspitno-obrazovnom radu ima najviše uslova predviđenih članom 6. ove odluke.

Član 7

PROFESOR RAZREDNE NASTAVE može dobiti nagradu "Sveti Sava" ako ispunjava sledeće uslove:

- da je u radnom odnosu u godini za koju se dodeljuje nagrada,

• da osmišljava podsticajnu sredinu za učenje. Usklađuje ciljeve, sadržaje, metode rada i očekivane ishode. Timski planira i programira obrazovno-vaspitni rad, uvažavajući individualne potrebe učenika, kao i potrebe roditelja i društvene zajednice,

• primenjuje raznovrsne metode, aktivnosti i oblike rada,

• individualizuje nastavu i podstiče motivaciju za učenje,

• predstavlja pozitivan model učenicima kako se misli i istražuje u disciplini koju predaje,

• primenjuje principe nenasilne komunikacije u izražavanju svojih zapažanja u vezi sa praćenjem i vrednovanjem učenika,

• zastupa najbolji interes učenika i gradi atmosferu međusobnog poverenja sa svim učesnicima u obrazovno-vaspitnom radu,

• sarađuje sa roditeljima i društvenom zajednicom i prihvata njihove inicijative u ostvarivanju zajedničkih interesa. Koristi mogućnost društvene zajednice za podsticanje razvoja dece,

• prikazuje novine stručnom organu ustanove iz naučne discipline, odnosno oblasti kojom se bavi,

• izvodi nastavu, organizuje predavanja, tribine, smotre, književne susrete, akademije ili izložbe u ustanovi, odnosno društvenoj sredini. Izrađuje nastavna sredstva. Objavljuje stručne radove u stručnim časopisima. Organizuje odlaske dece, učenika u pozorište, na koncert ili na sportsku manifestaciju. Angažuje se u radu stručnog aktiva, odnosno društva na nivou grada i šire. Učestvuje na stručnim skupovima, odnosno studijskim putovanjima,

• učestvuje u istraživačkom projektu u vezi unapređivanja obrazovno-vaspitne prakse. Koordinira ili vodi program ogleda. Učenici koje priprema imaju zapažene rezultate na opštinskom, okružnom i takmičenju (ukoliko učitelj vodi 4. razred, u suprotnom se ne uzima u obzir),

• vodi radionicu na stručnom skupu domaćeg ili međunarodnog karaktera,

• autor je istraživačkog projekta. Autor je ili koautor udžbenika ili zbirke zadataka koji su odobreni rešenjem Ministarstva za prosvete, nauke i tehnološkog razvoja, u skladu sa propisima iz oblasti obrazovanja i vaspitanja.

Da bi profesor razredne nastave dobio nagradu "Sveti Sava", ne mora kumulativno da ispunjava sve napred navedene uslove. Nagradu će dobiti onaj profesor razredne nastave koji u svom dosadašnjem stvaralačkom i vaspitno-obrazovnom radu ima najviše uslova predviđenih članom 7. ove odluke.

Član 8

VEROUČITELJ - KATIHETA može dobiti nagradu "Sveti Sava" ako ispunjava sledeće uslove:

- Da je u radnom odnosu u godini za koju se dodeljuje nagrada,

- Da svojim izgledom, ponašanjem i odnosom prema učenicima i zaposlenima u obrazovnoj ustanovi bude istinski svedok i misionar pravoslavne vere,

- Da osmišljava podsticajnu sredinu za učenje, usklađuje ciljeve, sadržaje i metode rada i očekivane ishode, timski planira i programira obrazovno-vaspitni rad, čuvajući potrebe učenika, roditelja i društvene zajednice,

- Primenjuje raznovrsne metode, aktivnosti i oblike rada, svakom učeniku prilazi kao autentičnoj ličnosti, te podstiče motivaciju za učenje i predstavlja pozitivan uzor učenicima kako se misli istražuje i živi u pravoslavnom duhu,

- Da izvodi nastavu, organizuje predavanja, tribine, smotre, duhovne večeri, akademije ili izložbe u ustanovi, odnosno društvenoj sredini, izrađuje nastavna sredstva, organizuje posete učenika pravoslavnim svetinjama, pravoslavnim duhovnim institucijama, kao i lokalitetima od pravoslavno-duhovnog i nacionalnog značaja, učestvuje na stručnim skupovima, odnosno studijskim putovanjima.

- Da bude aktivni član Pravoslavne Crkvene zajednice,

- Da živi liturgijskim, svetotajinskim životom,

- Da aktivno učestvuje u životu škole unoseći u njega iskustvo Crkve,

- Da učenici koje priprema, imaju zapažene rezultate na gradskom, okružnom i državnom takmičenju,

- Da kod učenika razvija osećaj religioznosti,

- Da učenike upoznaje i uči praktikovanju pravoslavnih verskih obreda, korišćenju molitve u svakodnevnom životu, upražnjavanju posta,

- Da kod učenika razvija i neguje stalno i redovno primanje Svetih Tajni Ispovesti i Pričešća,

- Da u kolektivu i sa učenicima organizuje i sprovodi humanitarne akcije, te da različitim aktivnostima kod dece razvija brigu o drugome i spremnost na međusobnu pomoć i saradnju,

- Da uzima učešća u projektima protiv nasilja i školi, te da uči Jevanđeljskom rešavanju sukoba,

- Da sa učenicima organizuje posete i pomoć bolesnima i starima,

- Da različitim vanškolskim aktivnostima organizuje obeležavanje i proslavu crkvenih praznika te tako kod učenika neguje čuvanje verskog i kulturnog identiteta,

- Da kod učenika razvija versku toleranciju,

- Da razume probleme savremenog doba i uključuje se u njihovo rešavanje, te da učenike upućuje na eshatološku dimenziju svih životnih događaja,

- Vodi radionicu na stručnom skupu domaćeg ili međunarodnog karaktera. Autor je istraživačkog projekta.

- Za dobijanje svetosavske nagrade veroučitelj - katiheta mora da ima blagoslov nadležnog Episkopa.

- Da bi veroučitelj-katiheta dobio nagradu "Sveti Sava", ne mora kumulativno da ispunjava sve napred navedene uslove. Nagradu će dobiti onaj veroučitelj katiheta koji u svom dosadašnjem obrazovno - vaspitnom, prosvetiteljskom i misionarskom radu ima najviše uslova predviđenih članom 8. ove odluke.

Član 9

NASTAVNIK PREDMETNE NASTAVE može dobiti nagradu "Sveti Sava" ako ispunjava sledeće uslove:

- da je u radnom odnosu u godini za koju se dodeljuje nagrada,

- da osmišljava podsticajnu sredinu za učenje, razvoj, usklađuje ciljeve, sadržaje, metode rada i očekivane ishode, timski planira i programira obrazovno-vaspitni rad, uvažavajući individualne potrebe učenika, kao i potrebe roditelja i društvene zajednice,

- primenjuje raznovrsne metode, aktivnosti i oblike rada, individualizuje nastavu i podstiče motivaciju za učenje, predstavlja pozitivan model učenicima kako se misli i istražuje u disciplini koju predaje,

- primenjuje principe nenasilne komunikacije u izražavanju svojih zapažanja u vezi sa praćenjem i vrednovanjem učenika,

- zastupa najbolji interes učenika i gradi atmosferu međusobnog poverenja sa svim učesnicima u obrazovno-vaspitnom radu,

- sarađuje sa roditeljima i društvenom zajednicom i prihvata njihove inicijative u ostvarivanju zajedničkih interesa, koristi mogućnosti društvene zajednice za podsticanje razvoja dece,

- prikazuje novine stručnom organu ustanove iz naučne discipline, odnosno oblasti kojom se bavi - izvodi nastavu, organizuje predavanja, tribine, smotre, književne susrete, akademije ili izložbe u ustanovi, odnosno društvenoj sredini; izrađuje nastavna sredstva, objavljuje stručne radove u stručnim časopisima; organizuje odlaske dece, učenika i studenata u bioskop, pozorište, na koncert ili na sportsku manifestaciju, angažuje se u radu stručnog aktiva, odnosno društva na nivou grada i šire; učestvuje na stručnim skupovima odnosno studijskim putovanjima,

- učestvuje u istraživačkom projektu u vezi unapređivanja obrazovno-vaspitne prakse. Koordinira ili vodi program ogleda.

- učenici koje priprema, imaju zapažene rezultate na gradskom, okružnom i državnom takmičenju,

- vodi radionicu na stručnom skupu domaćeg ili međunarodnog karaktera. Autor je istraživačkog projekta. Autor je ili koautor udžbenika ili zbirke zadataka koji su odobreni rešenjem Ministarstva prosvete, nauke i tehnološkog razvoja.

Da bi pojedinac dobio nagradu "Sveti Sava", ne mora kumulativno da ispunjava sve napred navedene uslove. Nagradu će dobiti onaj pojedinac koji u svom dosadašnjem stvaralačkom i vaspitno-obrazovnom radu ima najviše uslova predviđenih članom 9. ove odluke.

Član 10

Da bi NASTAVNIK PREDMETNE NASTAVE OSNOVNE ŠKOLE mogao dobiti nagradu "Sveti Sava" ako ispunjava sledeće uslove:

- da je u radnom odnosu u godini za koju se dodeljuje nagrada,

- da osmišljava podsticajnu sredinu za učenje, razvoj, usklađuje ciljeve, sadržaje, metode rada i očekivane ishode, timski planira i programira obrazovno-vaspitni rad, uvažavajući individualne potrebe učenika, kao i potrebe roditelja i društvene zajednice,

- primenjuje raznovrsne metode, aktivnosti i oblike rada, individualizuje nastavu i podstiče motivaciju za učenje, predstavlja pozitivan model učenicima kako se misli i istražuje u disciplini koju predaje,

- primenjuje principe nenasilne komunikacije u izražavanju svojih zapažanja u vezi sa praćenjem i vrednovanjem učenika,

- zastupa najbolji interes učenika i gradi atmosferu međusobnog poverenja sa svim učesnicima u obrazovno-vaspitnom radu,

- sarađuje sa roditeljima i društvenom zajednicom i prihvata njihove inicijative u ostvarivanju zajedničkih interesa, koristi mogućnosti društvene zajednice za podsticanje razvoja dece,

- prikazuje novine stručnom organu ustanove iz naučne discipline, odnosno oblasti kojom se bavi - izvodi nastavu, organizuje predavanja, tribine, smotre, književne susrete, akademije ili izložbe u ustanovi, odnosno društvenoj sredini; izrađuje nastavna sredstva, objavljuje stručne radove u stručnim časopisima; organizuje odlaske dece, učenika i studenata u bioskop, pozorište, na koncert ili na sportsku manifestaciju, angažuje se u radu stručnog aktiva, odnosno društva na nivou grada i šire; učestvuje na stručnim skupovima odnosno studijskim putovanjima,

- učestvuje u istraživačkom projektu u vezi unapređivanja obrazovno-vaspitne prakse. Koordinira ili vodi program ogleda.

- učenici koje priprema, imaju zapažene rezultate na gradskom, okružnom i državnom takmičenju,

- vodi radionicu na stručnom skupu domaćeg ili međunarodnog karaktera. Autor je istraživačkog projekta. Autor je ili koautor udžbenika ili zbirke zadataka koji su odobreni rešenjem Ministarstva prosvete, nauke i tehnološkog razvoja.

Da bi pojedinac dobio nagradu "Sveti Sava", ne mora kumulativno da ispunjava sve napred navedene uslove. Nagradu će dobiti onaj pojedinac koji u svom dosadašnjem stvaralačkom i vaspitno-obrazovnom radu ima najviše uslova predviđenih članom 10. ove odluke.

Član 11

Da bi PROFESOR PREDMETNE NASTAVE SREDNJE ŠKOLE mogao dobiti nagradu "Sveti Sava" ako ispunjava sledeće uslove:

- da je u radnom odnosu u godini za koju se dodeljuje nagrada,

- da osmišljava podsticajnu sredinu za učenje, razvoj, usklađuje ciljeve, sadržaje, metode rada i očekivane ishode, timski planira i programira obrazovno-vaspitni rad, uvažavajući individualne potrebe učenika, kao i potrebe roditelja i društvene zajednice,

- primenjuje raznovrsne metode, aktivnosti i oblike rada, individualizuje nastavu i podstiče motivaciju za učenje, predstavlja pozitivan model učenicima kako se misli i istražuje u disciplini koju predaje,

- primenjuje principe nenasilne komunikacije u izražavanju svojih zapažanja u vezi sa praćenjem i vrednovanjem učenika,

- zastupa najbolji interes učenika i gradi atmosferu međusobnog poverenja sa svim učesnicima u obrazovno-vaspitnom radu,

- sarađuje sa roditeljima i društvenom zajednicom i prihvata njihove inicijative u ostvarivanju zajedničkih interesa, koristi mogućnosti društvene zajednice za podsticanje razvoja dece,

- prikazuje novine stručnom organu ustanove iz naučne discipline, odnosno oblasti kojom se bavi - izvodi nastavu, organizuje predavanja, tribine, smotre, književne susrete, akademije ili izložbe u ustanovi, odnosno društvenoj sredini; izrađuje nastavna sredstva, objavljuje stručne radove u stručnim časopisima; organizuje odlaske dece, učenika i studenata u bioskop, pozorište, na koncert ili na sportsku manifestaciju, angažuje se u radu stručnog aktiva, odnosno društva na nivou grada i šire; učestvuje na stručnim skupovima odnosno studijskim putovanjima,

- učestvuje u istraživačkom projektu u vezi unapređivanja obrazovno-vaspitne prakse. Koordinira ili vodi program ogleda.

- učenici koje priprema, imaju zapažene rezultate na gradskom, okružnom i državnom takmičenju,

- vodi radionicu na stručnom skupu domaćeg ili međunarodnog karaktera. Autor je istraživačkog projekta. Autor je ili koautor udžbenika ili zbirke zadataka koji su odobreni rešenjem Ministarstva prosvete, nauke i tehnološkog razvoja.

Da bi pojedinac dobio nagradu "Sveti Sava", ne mora kumulativno da ispunjava sve napred navedene uslove. Nagradu će dobiti onaj pojedinac koji u svom dosadašnjem stvaralačkom i vaspitno-obrazovnom radu ima najviše uslova predviđenih članom 11. ove odluke.

Član 12

Da bi UČENIK OSNOVNE ŠKOLE iz člana 2. ove odluke mogao dobiti nagradu "Sveti Sava", mora da ispunjava sledeće uslove:

- da je redovan učenik osnovne škole,

- da ima prebivalište na teritoriji grada Vranja,

- da je odličan učenik,

- da ima primerno vladanje u školi,

- da je osvojio 1.,2.ili 3. nagradu na opštinskom takmičenju u prethodnoj godini,

- da je osvojio 1.,2.ili 3. nagradu na okružnom takmičenju u prethodnoj godini,

- da je osvojio 1., 2., ili 3. nagradu na republičkom takmičenju u organizaciji Ministarstva prosvete u prethodnoj godini,

- da je osvojio 1., 2. ili 3. nagradu na međunarodnom takmičenju (Olimpijada ili Balkanijada) u prethodnoj godini,

- da je osvojio 1., 2. ili 3. nagradu na međunarodnim konkursima u oblasti umetnosti u prethodnoj godini.

Ukoliko više kandidata za nagradu ima isti učinak na republičkim takmičenjima, uzimaju se u obzir i prethodni nivoi takmičenja, kako njihov broj, tako i osvojena mesta.

Da bi učenik dobio nagradu "Sveti Sava" ne mora kumulativno da ispunjava sve napred navedene uslove. Nagradu će dobiti učenik koji u prethodnoj godini ispunjava najveći broj uslova predviđenih članom 12. ove odluke.

Član 13

Da bi UČENIK SREDNJE ŠKOLE iz člana 2. ove odluke mogao dobiti nagradu "Sveti Sava", mora da ispunjava sledeće uslove:

- da je redovan učenik srednje škole,

- da ima prebivalište na teritoriji grada Vranja,

- da je odličan učenik,

- da ima primerno vladanje u školi,

- da je osvojio 1.,2. ili 3. nagradu na opštinskom takmičenju u prethodnoj godini,

- da je osvojio 1.,2. ili 3. nagradu na okružnom takmičenju u prethodnoj godini,

- da je osvojio 1., 2., ili 3. nagradu na republičkom takmičenju u organizaciji Ministarstva prosvete u prethodnoj godini,

- da je osvojio 1., 2. ili 3. nagradu na međunarodnom takmičenju (Olimpijada ili Balkanijada) u prethodnoj godini,

- da je osvojio 1., 2. ili 3. nagradu na međunarodnim konkursima u oblasti umetnosti u prethodnoj godini.

Ukoliko više kandidata za nagradu ima isti učinak na republičkim takmičenjima, uzimaju se u obzir i prethodni nivoi takmičenja, kako njihov broj, tako i osvojena mesta.

Da bi učenik dobio nagradu "Sveti Sava" ne mora kumulativno da ispunjava sve napred navedene uslove. Nagradu će dobiti učenik koji u prethodnoj godini ispunjava najveći broj uslova predviđenih članom 13. ove odluke.

Član 14

Da bi STUDENT iz člana 2. ove odluke mogao dobiti nagradu "Sveti Sava", mora da ispunjava sledeće uslove:

- da je redovan student poslednje dve godine prvog stepena osnovnih akademskih ili strukovnih studija na svom fakultetu ili visokoj školi,

- da ima prebivalište na teritoriji grada Vranja,

- da ima srednju ocenu najmanje 9,25 tokom dosadašnjih studija,

- da je dao izuzetan doprinos razvoju nauke u prethodnoj godini (učešće na međunarodnim simpozijumima, međunarodnim konkursima i takmičenjima, objava radova u stručnim i naučnim časopisima)

- da je učestvovao u projektima u saradnji sa organizacijama i institucijama iz oblasti privrede, sporta, kulture, zdravlja i drugih.

Da bi student dobio nagradu "Sveti Sava", ne mora kumulativno da ispunjava sve napred navedene uslove. Nagradu će dobiti onaj student koji u prethodnoj godini ispunjava najveći broj uslova predviđenih ovim članom Odluke.

Član 15

Nagrađeni pojedinac, ustanova, vaspitač, profesor razredne nastave, veroučitelj-katiheta, nastavnik predmetne nastave u osnovnoj školi, profesor predmetne nastave u srednjoj školi, učenik ili student mogu ponovo dobiti nagradu "Sveti Sava" ukoliko imaju izuzetne rezultate u svom radu.

III KRITERIJUMI ZA DODELU NAGRADE "SVETI SAVA"

Član 16

1. Rad ostvaren u radnom odnosu:

Za svaku godinu rada ostvarenog u radnom odnosu u ustanovama obrazovanja, i to:

• Do 10 godina staža - 2 boda

• Do 20 godina staža - 4 boda

• Do 30 godina staža - 6 bodova

• Preko 30 godina staža - 8 bodova

2. Takmičenja:

1) Broj bodova za opštinsko takmičenje :

• Za osvojeno prvo mesto - 2 boda,

• Za osvojeno drugo mesto - 1,5 bod,

• Za osvojeno treće mesto - 1 bod;

2) Broj bodova za okružno/ regionalno, odnosno gradsko takmičenje :

• Za osvojeno prvo mesto - 4 boda,

• Za osvojeno drugo mesto - 3 boda

• Za osvojeno treće mesto - 2 boda;

3) Broj bodova sa republičkog takmičenja:

• Za osvojeno prvo mesto - 8 bodova

• Za osvojeno drugo mesto - 6 bodova

• Za osvojeno treće mesto - 4 boda;

4) Broj bodova za međunarodno takmičenje:

• Za osvojeno prvo mesto - 15 bodova,

• Za osvojeno drugo mesto - 12 bodova

• Za osvojeno treće mesto - 10 bodova.

Po osnovu takmičenja boduje se i nastavnik-profesor koji je učenika pripremao za takmičenje i koji je ostvario relevantne rezultate.

Vrednuje se samo jedan rezultat ostvaren u najvišem rangu po takmičenju.

Ukoliko dva ili više kandidata imaju isti broj bodova po svim kriterijumima, Odbor može predložiti Gradskom veću da se dodeli još jedna nagrada.

3. Pedagoški doprinos u radu:

1) Rad na izradi udžbenika koji su odobreni rešenjem ministra prosvete, nauke i tehnološkog razvoja, u skladu sa propisima iz oblasti obrazovanja i vaspitanja:

- autor 7 bodova,

- saradnik na izradi udžbenika ilustrator - 5 bodova

- recenzent - 4 boda

- autor knjige gde je izdavač škola 7 bodova

- koautor - 5 bodova

2) objavljen rad iz struke u stručnoj domaćoj ili stranoj literaturi - 1 bod (bez obzira na broj objavljenih radova - po ovom osnovu dobija se samo 1 bod).

IV PODNOŠENJE PREDLOGA ZA NAGRADU

Član 17

Pravo predlaganja kandidata za nagradu "Sveti Sava" imaju sva pravna i fizička lica.

Fizičko ili pravno lice koje predlaže kandidata za nagradu, može predložiti samo po jednu ustanovu, pojedinca, učenika ili studenta.

Svaki predlagač može predložiti samo po jednog kandidata za svaku pojedinačnu kategoriju.

Član 18

Predlozi moraju biti dostavljeni u pisanoj formi i sa potrebnim dokazima (fotokopije dokumenata), sa potpunim obrazloženjem, tako da Komisija ima dovoljno elemenata za ocenu predloženih kandidata.

Sva dokumenta moraju biti vezana isključivo za godinu za koju se dodeljuje nagrada.

Član 19

Konkurs se dostavlja svim obrazovno-vaspitnim ustanovama i objavljuje se na zvaničnom sajtu Grada u roku od 15 dana.

Predlozi se dostavljaju Komisiji za utvrđivanje predloga za dodelu nagrade "Sveti Sava" najkasnije do 15. januara tekuće godine Odseku za obrazovanje, kulturu, omladinu, sport i informisanje Gradske uprave grada Vranja.

Zahtevi koji stignu posle utvrđenog roka iz prethodnog stava ovog člana, neće biti uzeti u razmatranje.

V ODLUČIVANJE

Član 20

Predlog za dodeljivanje nagrade "Sveti Sava", na osnovu kriterijuma iz ove Odluke, utvrđuje Komisija koja se sastoji od pet članova i njihovih zamenika koje imenuje Odbor.

Mandat predsednika i članova Komisije traje za vreme trajanja manifestacije.

Sve administrativne i tehničke poslove za potrebe Komisije obavlja stručni saradnik za obrazovanje pri Odseku za obrazovanje, kulturu, omladinu, sport i informisanje grada Vranja.

Član 21

Komisija radi u sednicama.

Konstitutivnu sednicu saziva i njenim radom rukovodi stručni saradnik Odseka za obrazovanje, kulturu, sport, omladinu i informisanje grada Vranja do izbora Predsednika komisije.

Predsednika i zamenika predsednika komisije biraju članovi Komisije iz svojih redova.

Komisija donosi predlog odluke većinom glasova članova Komisije.

Član 22

Komisija utvrđuje predlog Odluke o dodeljivanju nagrade "Sveti Sava" i uz odgovarajuće obrazloženje dostavlja ga Odboru na razmatranje i odlučivanje.

Član 23

Odluku o dodeli nagrade "Sveti Sava" ustanovi, vaspitaču, profesoru razredne nastave, veroučitelju-katiheti, nastavniku predmetne nastave osnovne škole, profesoru predmetne nastave srednje škole, učeniku/ci osnovne škole, učeniku/ci srednje škole i studentu donosi Organizacioni odbor manifestacije "Svetosavska nedelja", kvalifikovanom većinom glasova članova Odbora.

Ako Odbor, na predlog Komisije oceni da nema kandidata koji ispunjavaju uslove predviđene Odlukom za dodelu nagrade "Sveti Sava" u nekoj kategoriji, te godine nagrada u toj kategoriji neće biti dodeljena.

Član 24

Predsednik Odbora uručuje nagradu "Sveti Sava" dobitnicima na svečan način, u okviru proslave manifestacije "Svetosavska nedelja".

VI ZAVRŠNE ODREDBE

Član 25

Odluka o dodeljivanju nagrade "Sveti Sava" sa obrazloženjem, objavljuje se na zvaničnom sajtu grada Vranja i u sredstvima javnog informisanja.

Član 26

Stručne i administrativno-tehničke poslove za potrebe Odbora i Komisije za utvrđivanje predloga za dodelu nagrade "Sveti Sava" obavlja Odsek za obrazovanje, kulturu, omladinu, sport i informisanje grada Vranja.

Član 27

Ova odluka stupa na snagu osmog dana od dana objavljivanja u "Službenom glasniku grada Vranja".

Stupanjem na snagu ove odluke, prestaje da važi Pravilnik o dodeljivanju nagrade "Sveti Sava" ("Sl. glasnik grada Vranja", br. 24/2014).