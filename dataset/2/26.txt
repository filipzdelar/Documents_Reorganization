I OSNOVNE ODREDBE

Član 1

Ovom odlukom bliže se određuje način upotrebe simbola grada Valjeva.

Simboli grada Valjeva su grb i zastava.

Član 2

Simbolima grada se predstavlja grad Valjevo.

Simbolima grada ne smeju se vređati opšti i državni interesi, nacionalna i verska osećanja i javni moral, ugled i dostojanstvo grada Valjeva.

Simboli grada mogu se upotrebljavati samo u obliku i sa sadržinom koji su utvrđeni Odlukom o utvrđivanju simbola grada Valjeva ("Službeni glasnik grada Valjeva", broj 10/10).

II ZAJEDNIČKE ODREDBE

Član 3

Simboli grada Valjeva ne mogu se upotrebljavati kao robni ili uslužni žig, uzorak ili model, niti kao bilo koji drugi znak za obeležavanje robe ili usluge.

Simboli grada Valjeva ne smeju se upotrebljavati ako su oštećeni ili su svojim izgledom nepodobni za upotrebu, već se povlače iz upotrebe.

Na simbolima grada Valjeva ne može se ništa ispisivati, odnosno upisivati, niti se oni mogu menjati.

Izuzetno, ako je to posebnim aktom Skupštine grada Valjeva propisano, simboli grada Valjeva mogu se upotrebiti kao sastavni deo drugih amblema, odnosno znakova.

Član 4

Simboli grada Valjeva mogu se upotrebljavati u umetničkom stvaralaštvu, nastavnom i obrazovnom radu, pod uslovom da njihova upotreba ne bude u suprotnosti sa članovima 2. i 3. ove odluke.

Član 5

Etalon (izvornik) simbola grada Valjeva je knjiga grafičkih standarda simbola grada Valjeva koja se izrađuje na osnovu opisa (blazona) iz Odluke o utvrđivanju simbola grada Valjeva ("Službeni glasnik grada Valjeva", broj 10/10), a utvrđuje se aktom Gradskog veća grada Valjeva.

Oblikovanje za upotrebu Velikog i Malog grba grada Valjeva i zastave grada Valjeva vrši se u skladu sa aktom iz stava 1. ovog člana.

Etalon (izvornik) simbola grada Valjeva čuva se u Gradskoj upravi za lokalni razvoj, privredu, urbanizam i komunalne poslove - Odeljenju za zajedničke poslove.

III UPOTREBA GRBA GRADA VALJEVA

Član 6

Veliki grb grada Valjeva se ističe na zgradi Grada i koriste ga: Gradonačelnik i zamenik Gradonačelnika grada Valjeva, predsednik, zamenik i sekretar Skupštine grada Valjeva i članovi Gradskog veća grada Valjeva.

Veliki grb grada Valjeva se koristi u najsvečanijim prilikama kada se ističu značaj i tradicija grada i obavezno se utiskuje, odnosno štampa na poveljama, diplomama i drugim javnim priznanjima koje dodeljuje grad Valjevo.

Mali grb grada Valjeva kao simbol grada Valjeva koriste organi grada za obeležavanje svojih prostorija.

Mali grb grada Valjeva mogu službeno da koriste javne službe čiji je osnivač grad Valjevo.

Mali grb grada Valjeva koriste odbornici Skupštine grada Valjeva, gradske uprave grada Valjeva i pomoćnici Gradonačelnika grada Valjeva.

Član 7

Na službenim vizitkartama Gradonačelnika i zamenika Gradonačelnika grada Valjeva, predsednika, zamenika predsednika i sekretara Skupštine grada Valjeva i članova Gradskog veća grada Valjeva otiskuje se Veliki grb grada Valjeva.

Na službenim vizitkartama odbornika Skupštine grada Valjeva, pomoćnika Gradonačelnika grada Valjeva, zaposlenih lica u gradskim upravama otiskuje se Mali grb grada Valjeva.

Mali grb grada Valjeva utiskuje se odnosno štampa na predmetima koji se daju u reprezentativne svrhe, kao i na memorandumima, kovertama, zvaničnim pozivnicama i čestitkama, vizitkartama i slično.

Član 8

Mali grb grada Valjeva i Veliki grb grada Valjeva se ističe na javnim mestima, na otvorenom i u zatvorenom prostoru za vreme manifestacija koje su od značaja za grad.

Mali grb grada Valjeva i Veliki grb grada Valjeva mogu koristiti predstavnici grada Valjeva u zvaničnim posetama u zemlji i u inostranstvu.

Član 9

Mali grb grada Valjeva može se koristiti kao oznaka na službenom odelu zaposlenih u gradskim upravama, koji u skladu sa posebnim propisima nose službena odela, kao i na službenim vozilima koja koriste zaposleni u organima grada.

Član 10

Po pravilu Mali grb grada Valjeva se ističe na prilaznim putevima, na granici teritorije grada Valjeva.

IV SAGLASNOST NA UPOTREBU MALOG GRBA GRADA VALJEVA

Član 11

Pravnom licu i preduzetniku sa sedištem na teritoriji grada Valjeva izuzetno se može dati saglasnost da Mali grb grada Valjeva upotrebi kao ukras ili u komercijalne svrhe.

Skupština grada Valjeva daje posebnim aktom saglasnost na upotrebu grba.

Član 12

Zahtev za davanje saglasnosti sadrži namenu upotrebe grba, tehnički opis, rok za koji se saglasnost za upotrebu traži, kao i druge podatke bitne za odlučivanje.

Član 13

Aktom kojim se daje saglasnost na upotrebu Malog grba grada Valjeva mogu se odrediti posebni uslovi kao što su: namena, rok u kome saglasnost važi i drugo, ukoliko takva ograničenja odgovaraju nameni upotrebe grba.

Na izdati akt plaća se gradska administrativna taksa utvrđena posebnom odlukom Skupštine grada Valjeva.

Član 14

Subjekti kojima je data saglasnost na upotrebu Malog grba grada Valjeva dužni su da obezbede doličan izgled ukrasa, odnosno predmeta koji se koristi u komercijalne svrhe.

Član 15

Akt kojim je data saglasnost na upotrebu Malog grba grada Valjeva ukida se ako korisnik saglasnosti upotrebljava grb suprotno utvrđenoj nameni i uslovima ili se ne stara o doličnom izgledu ukrasa odnosno predmeta koji se koriste u komercijalne svrhe.

Akt kojim se odbija zahtev za davanje saglasnosti, odnosno akt kojim se ukida data saglasnost je konačan.

Član 16

Gradska uprava za lokalni razvoj, privredu, urbanizam i komunalne poslove - Odeljenje za poslove organa grada vodi evidenciju akata iz člana 11. stav 2. ove odluke i člana 15. stav 2. ove odluke.

V UPOTREBA ZASTAVE GRADA VALJEVA

Član 17

Zastava grada Valjeva se ističe:

1) na službenim zgradama koje koriste organi grada,

2) u dane praznika grada na zgradama javnih preduzeća i ustanova koje je osnovao grad Valjevo,

3) prilikom proslava, svečanosti i drugih masovnih kulturnih, sportskih i sličnih manifestacija od značaja za grad,

4) prilikom međugradskih susreta, takmičenja i drugih skupova (političkih, naučnih, kulturno-umetničkih, sportskih i dr.) na kojima grad učestvuje ili je reprezentovan, u skladu sa pravilima i praksom održavanja tih skupova,

5) u drugim slučajevima, ako njena upotreba nije u suprotnosti sa odredbama ove odluke.

Zastava grada Valjeva stalno je postavljena u službenim prostorijama Gradonačelnika, zamenika i pomoćnika Gradonačelnika grada Valjeva, predsednika, zamenika predsednika i sekretara Skupštine grada Valjeva, članova Gradskog veća i načelnika gradskih uprava.

Član 18

Zastava grada ne sme biti postavljena tako da dodiruje tlo, niti kao podloga, podmetač, prostirka, zavesa ili draperija.

Zastavom se ne mogu prekrivati vozila ili drugi predmeti, niti se mogu ukrašavati konferencijski stolovi ili govornice, osim u formi stone zastavice.

Ako se zastava ističe na govornici, može se postaviti na koplju ili na zidu govornice, tako da je govornik ne zaklanja.

Član 19

Zastava se po pravilu vije sa jarbola ili koplja.

U dane žalosti zastava se vije na pola koplja.

Kada se zastava ističe zajedno sa zastavom drugih gradova, postavlja se tako da se ističe prednost zastave grada kao domaćina.

Član 20

Grad Valjevo ima i naročito unikatnu zastavu - standartu koja se čuva u zgradi organa grada Valjeva.

Standarta je izrađena na posebno dekorativan način i ukrašena zlatnim resama po slobodnim rubovima.

Standarta simboliše grad Valjevo i koristi se u najsvečanijim prilikama u prisustvu Gradonačelnika i zamenika Gradonačelnika grada Valjeva i predsednika i zamenika predsednika Skupštine grada Valjeva.

Izuzetno i po odobrenju Gradonačelnika, standarta se može nositi u procesiji i izvan zgrade organa grada Valjeva.

VI NADZOR

Član 21

O sprovođenju ove odluke stara se Gradska uprava za lokalni razvoj, privredu, urbanizam i komunalne poslove - Odeljenje za zajedničke poslove.

Nadzor nad sprovođenjem odredaba ove odluke vrše Gradska uprava za društvene delatnosti, finansije, imovinske i inspekcijske poslove - Odeljenje za inspekcijske poslove i Gradska uprava za lokalni razvoj, privredu, urbanizam i komunalne poslove - Odeljenje komunalne policije.

Inspekcijski nadzor nad sprovođenjem odredaba ove odluke vrši Gradska uprava za društvene delatnosti, finansije, imovinske i inspekcijske poslove - Odeljenje za inspekcijske poslove.

Komunalni inspektor ovlašćen je da, ukoliko utvrdi da se simboli grada koriste suprotno odredbama ove odluke, rešenjem zabrani upotrebu simbola i naloži uklanjanje sa objekata i predmeta na kojima su istaknuti i da preduzme druge mere radi otklanjanja nedostataka.

VII KAZNENE ODREDBE

Član 22*

Novčanom kaznom od 50.000 do 1.000.000 dinara kazniće se za prekršaj pravno lice ako:

1. koristi simbole grada Valjeva suprotno članu 2. stav 3, članu 3, 4. i 6,

2. koristi grb grada Valjeva bez saglasnosti iz člana 11. stav 2,

3. koristi grb grada Valjeva suprotno aktu iz člana 13,

4. koristi grb grada Valjeva suprotno članu 14,

5. koristi zastavu grada Valjeva suprotno članu 18.

Za prekršaj iz stava 1. ovog člana kazniće se odgovorno lice i fizičko lice novčanom kaznom od 5.000 do 75.000 dinara.

Za prekršaj iz stava 1.ovog člana kazniće se preduzetnik novčanom kaznom od 10.000 do 250.000 dinara.

VIII ZAVRŠNA ODREDBA

Član 23

Ova odluka stupa na snagu osmog dana od dana objavljivanja u "Službenom glasniku grada Valjeva."

 

Samostalni član Odluke o dopuni
Odluke o načinu upotrebe simbola grada Valjeva

("Sl. glasnik grada Valjeva", br. 11/2016 - sveska 1)

Član 2

Ova odluka stupa na snagu narednog dana od dana objavljivanja u "Službenom glasniku grada Valjeva".