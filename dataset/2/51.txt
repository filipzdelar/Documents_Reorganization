Član 1

Ovom odlukom pristupa se izradi simbola grada Loznice.

Simboli grada Loznice su grb (Osnovni, Srednji i Veliki grb) i zastava (u daljem tekstu: simboli grada).

Sadržina grba i zastave mora odgovarati istorijskim i stvarnim činjenicama vezanim za grad Loznicu.

Član 2

Ovlašćuje se Centar za kulturu "Vuk Karadžić" Loznica (u daljem tekstu: Centar za kulturu) da organizuje i sprovede postupak izrade simbola grada.

Član 3

U smislu člana 2. ove odluke Centar za kulturu će odrediti opšte karakteristike simbola grada u skladu sa odredbama člana 1. stav 3. ove Odluke.

Postupak izbora simbola grada vršiće se na osnovu pozivnog konkursa.

Uslove pozivnog konkursa, konkursni zadatak, listu pozivnih subjekata, pregled prispelih radova i njihovu usklađenost sa konkursnim zadatkom utvrdiće odnosno vrši Centar za kulturu.

Po sprovedenom konkursu Centar za kulturu dostavlja Izveštaj o sprovedenom postupku izrade simbola grada Gradskom veću grada Loznice, sa naznakom koji od prispelih radova ispunjavaju uslove konkursa i predlogom za izbor simbola grada.

Pre dostavljanja Izveštaja iz prethodnog stava Centar za kulturu će o prispelim radovima koji ispunjavaju uslove konkursa i predlogu za izbor simbola grada pribaviti mišljenje ovlašćenih predstavnika odborničkih grupa konstituisanih u Skupštini grada Loznice.

Član 4

Na osnovu Izveštaja iz člana 3. stav 4. ove odluke Gradsko veće grada Loznice utvrdiće Predlog Odluke o simbolima grada i načinu njihove upotrebe.

Član 5

Administrativno tehničku podršku u postupku izrade simbola grada Centru za kulturu će pružiti Gradska uprava - Odeljenje za društvene delatnosti koje će izraditi nacrt Odluke o simbolima grada i načinu njihove upotrebe.

Član 6

Stupanjem na snagu ove odluke prestaje da važi Odluka o izradi simbola grada Loznice i Rešenje o imenovanju komisije za sprovođenje postupka izbora ponuđača za izradu simbola grada Loznice ("Službeni list grada Loznice" broj 10/10).

Član 7

Ova odluka stupa na snagu osmog dana od dana objavljivanja u "Službenom listu grada Loznice".