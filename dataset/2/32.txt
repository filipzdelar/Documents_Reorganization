Član 1

Ovim rešenjem obrazuje se Komisija za utvrđivanje izgleda i sadržaja javnih priznanja grada Vranja.

Član 2

U Komisiju se imenuju:

1. Dr. Slobodan Stamenković, predsednik Skupštine grada Vranja.

2. Zoran Antić, zamenik predsednika Skupštine grada Vranja.

3. Siniša Mitić, član Gradskog veća Grada Vranja za resor državna uprava i lokalna samouprava.

4. Zoran Najdić, član Gradskog veća Grada Vranja za resor kultura i informisanje.

5. Slađan Ivanović, direktor Javne ustanove - Narodni muzej u Vranju.

6. Danijela Đorđević, direktor Javne ustanove - Narodna biblioteka "Bora Stanković" u Vranju.

7. Nebojša Cvetković. v.d. direktora Javne ustanove - Pozorište "Bora Stanković" u Vranju.

8. Suzana Petrović, direktor Javne ustanove - Istorijski arhiv "31. januar" u Vranju.

9. Nebojša Veličković, direktor Javne ustanove - Narodni univerzitet u Vranju.

10. Vesna Miletić, šef Kabineta Gradonačelnika.

Član 3

Zadatak radne grupe je da utvrdi izgled i sadržaj javnih priznanja grada Vranja, ustanovljenih Odlukom o javnim priznanjima grada Vranja, broj: 17-25/09-12 od 26.11.2009. godine ("Službeni glasnik Grada Vranja", broj: 29/09), i to:

1. Zvanja "Počasni građanin Vranja";

2. "Povelje zahvalnosti";

3. "Plakete 31. januar";

4. "Specijalnog javnog priznanja 31. januar";

5. "Javnog priznanja 31. januar";

6. "Javnog priznanja 7. septembar";

7. "Javnog priznanja 4. oktobar"; i

8. "Zahvalnice".

Član 4

Rešenje stupa na snagu danom donošenja.

Član 5

Rešenje objaviti u "Službenom glasniku Grada Vranja".