I OSNOVNE ODREDBE

Član 1

Ovom Odlukom ustanovljavaju se javna priznanja koja dodeljuje Grad Sombor, uređuje se postupak, uslovi, kriterijumi i način dodele, kao i oblik i izgled javnih priznanja.

Član 2

Javna priznanja se dodeljuju organizacijama i građanima za značajna ostvarenja u privredi, nauci, umetnosti, sportu, obrazovno vaspitnom radu i drugim društvenim oblastima koje doprinose razvoju Grada Sombora, njegovom napretku i prosperitetu.

Javna priznanja Grada Sombora su:

- Oktobarska nagrada Grada Sombora

- Povelja Grada Sombora

- Povelja Počasni građanin Grada Sombora

- Povelja ambasador grada Sombora

II OKTOBARSKA NAGRADA GRADA SOMBORA

Član 3

Oktobarska nagrada grada Sombora se dodeljuje kao posebno priznanje za izuzetne rezultate u višegodišnjem radu, odnosno kao nagrada za životno delo.

Oktobarska nagrada se dodeljuje pojedincu za rezultate iz oblasti stvaralaštva i rada utvrđenih u članu 4. Ove Odluke.

Oktobarska nagrada se istom licu može dodeliti samo jedanput.

III POVELJA GRADA SOMBORA

Član 4

Povelja Grada Sombora se dodeljuje kao društveno priznanje za postignute rezultate i dostignuća u prethodnoj godini.

Povelja Grada Sombora se može dodeliti građaninu, grupi građana, preduzećima, ustanovama, državnim organima, mesnim zajednicama, udruženjima i drugim društvenim organizacijama sa i van teritorije grada Sombora za postignute rezultate koja su od opšteg značaja za Grad Sombor i koja doprinose razvoju, očuvanju tradicije i afirmaciji Grada Sombora i to u oblastima:

- Nauke, kulture i umetnosti, odnosno svih vidova stvaralaštva (književnog, muzičkog, scenskog, rtv, likovnog, primenjene umetnosti i sl.)

- Planiranja i uređenja prostora i naselja, arhitekture i građevinarstva

- Unapređenja i zaštite životne sredine

- Prosvete i razvoja školstva

- Sporta i postignutih sportskih rezultata

- Zdravstva i zdravstvene zaštite

- Socijalnog i humanitarnog rada

- Privrednog razvoja i drugih nenavedenih oblasti od značaja za grad Sombor.

Povelja se može dodeliti i stranim i domaćim institucijama, opštinama, gradovima i pojedincima za ostvarene rezultate u oblasti međuopštinske i međunarodne saradnje.

Član 5

Dobitniku Povelje može se ponovo dodeliti Povelja ukoliko Gradsko veće oceni da je fizičko, odnosno pravno lice ponovo ostvarilo izvanredne rezultate i dostignuća u radu.

IV ZVANJE POČASNOG GRAĐANINA

Član 6

Zvanje Počasni građanin Grada Sombora se može dodeliti građaninu Republike Srbije, kao i stranom državljaninu, državniku i funkcioneru međunarodne organizacije i udruženja, nevladine organizacije i drugih oblika civilnog sektora, državnog organa ili organa jedinice lokalne samouprave, istaknutoj ličnosti i drugom pojedincu, koji svojim radom, naučnim, umetničkim, političkim, humanitarnim i drugim oblicima delovanja, doprinese razvoju i ugledu grada Sombora.

Član 7

Zvanje Počasni građanin grada Sombora može se dodeliti za izuzetan doprinos:

- razvoju Grada u različitim oblastima

- afirmaciji Grada u zemlji i inostranstvu

- afirmaciji vrednosti koje su utemeljene u istorijskoj ulozi grada Sombora i njegovom značenju u razvoju moderne srpske države i doprinosu dobrobiti njegovih žitelja

- u pružanju humanitarne pomoći i donacija

- unapređenju i zaštiti životne sredine

- razvoju demokratije

- zaštiti i unapređenju ljudskih prava i sloboda i

- unapređenju i razvoju lokalne samouprave

Član 8

Predlog za dodelu zvanja Počasni građanin Grada Sombora, utvrđuje Gradsko veće grada Sombora.

Odluku o dodeli zvanja Počasni građanin donosi Skupština grada Sombora uz prethodnu saglasnost Ministarstva nadležnog za poslove lokalne samouprave.

Licu kome je dodeljeno zvanje Počasni građanin uručuje se Povelja počasnog građanina grada Sombora i simbolični ključ kapije Grada Sombora.

Član 9

Zvanje Počasni građanin grada Sombora je protokolarno priznanje i njime se ne stiču posebna prava, niti obaveze.

V ZVANJE AMBASADORA GRADA SOMBORA

Član 10

Zvanje ambasadora grada Sombora se može dodeliti najreprezentativnijem pojedincu koji je ostvario izvanredne rezultate u oblasti nauke, kulture, sporta, umetnosti i druga izuzetna dostignuća, rođenom u gradu Somboru, koji je ostvario međunarodno priznate rezultate i koji je prepoznao i u svom dosadašnjem radu promovisao vrednosti koje grad Sombor podržava i za koje se zalaže.

Predlog za dodelu zvanja ambasador Grada Sombora, utvrđuje Gradsko veće grada Sombora, na osnovu obrazloženog predloga fizičkog ili pravnog lica i inicijative gradonačelnika grada Sombora.

Odluku o dodeli zvanja ambasador grada Sombora donosi Skupština grada Sombora.

Licu kome je dodeljeno zvanje ambasador grada Sombora uručuje se Povelja ambasadora grada Sombora i dodeljuje pločica na Trgu umetnosti.

VI POSTUPAK DODELE JAVNIH PRIZNANJA

Član 11

Oktobarska nagrada grada Sombora se dodeljuje na svečanosti povodom dana obeležavanja oslobođenja Grada Sombora od fašističke okupacije 21. oktobra.

Povelja Grada Sombora, Povelja Počasni građanin Grada Sombora i Povelja ambasadora grada Sombora se dodeljuju na svečanosti, povodom proslave Dana Grada 17. Februara, kada je Sombor stekao status slobodnog kraljevskog grada.

Član 12

Oktobarska nagrada i Povelja grada Sombora se dodeljuju u vidu povelje i u novčanom iznosu.

Visinu novčanog iznosa određuje Gradsko veće.

Zvanje Počasni građanin se dodeljuje u vidu povelje i simboličnog ključa kapije Grada Sombora.

Zvanje ambasadora grada Sombora se dodeljuju u vidu povelje i pločice na Trgu umetnosti.

Godišnje može da se dodeli jedna Povelja grada Sombora, jedna Oktobarska nagrada, jedna Povelja Počasni građanin Grada Sombora i jedna Povelja ambasadora grada Sombora.

Oktobarska nagrada i Povelja ambasadora grada Sombora se mogu dodeliti i posthumno.

Član 13

Javna priznanja obavezno sadrže grb Grada Sombora, naziv priznanja, mesto predviđeno za upisivanje podataka o dobitniku, mesto, datum donošenja, potpis gradonačelnika i pečat.

Tekst Javnih priznanja se štampa na srpskom jeziku ćiriličnim pismom.

Izgled Javnih priznanja, na predlog Komisije iz člana 12. ove Odluke utvrđuje Gradsko veće.

Komisija iz prethodnog stava može za izradu idejnog rešenja Javnog priznanja raspisati konkurs.

Član 14

Gradsko veće obrazuje Komisiju za javna priznanja (u daljem tekstu Komisija).

Komisija iz prethodnog stava ima predsednika i četiri člana. Predsednik i dva člana su iz reda odbornika Skupštine grada, a dva člana su iz reda istaknutih građana koji uživaju ugled u oblastima svog rada i delovanja.

Mandat Komisije traje četiri godine.

Komisija objavljuje Javni poziv, sprovodi postupak i utvrđuje predlog za dodelu javnih priznanja.

Član 15

Komisija svake godine objavljuje Javni poziv za dostavljanje predloga za dodelu javnih priznanja grada Sombora.

Javni poziv se objavljuje na sajtu Grada, putem sredstava informisanja i na drugi pogodan način najkasnije 30 dana pre krajnjeg roka za dostavu predloga.

Član 16

Predlog za dodelu javnih priznanja mogu podneti pojedinci, grupe građana i institucije, odnosno pravna i fizička lica sa sedištem na teritoriji grada Sombora.

Predlog se podnosi Komisiji u pisanoj formi sa obrazloženjem koje sadrži biografske podatke o kandidatu, podatke o delu koje se predlaže i rezultate rada kandidata, kao i navođenje adrese stanovanja za fizička lica i adrese sedišta za pravna lica.

Predlozi za dodelu Povelje Grada Sombora, Povelje Počasni građanin Grada Sombora i Povelje ambasadora grada Sombora se dostavljaju Komisiji najkasnije do 31 decembra u godini za koju se dodeljuje priznanje, a za dodelu Oktobarske nagrade najkasnije do 15 septembra tekuće godine.

Član 17

Ukoliko nije bilo predloga za dodelu javnih priznanja javna priznanja se mogu dodeliti na predlog Gradonačelnika i članova Komisije, primenom kriterijuma i uslova utvrđenih ovom Odlukom.

Član 18

Odluku o dodeli Oktobarske nagrade i Povelju grada Sombora donosi Gradsko veće grada Sombora na predlog koji je utvrdila Komisija za javna priznanja.

Odluku o dodeli Povelje Počasni građanin grada Sombora i Povelje ambasador grada Sombora, donosi Skupština grada Sombora na predlog koji je utvrdilo Gradsko veće grada Sombora.

Javna priznanja uručuje gradonačelnik grada Sombora, zamenik gradonačelnika ili lice koje ovlasti gradonačelnik.

Član 19

Pored javnih priznanja ustanovljenih ovom Odlukom u oblasti obrazovanja se dodeljuje posebno priznanje "Avram Mrazović".

Postupak, uslovi i kriterijumi za dodelu priznanja iz stava 1. Ovog člana se utvrđuju posebnom Odlukom.

Član 20

Sredstva za izradu i dodelu javnih priznanja obezbeđuju se u budžetu grada.

Član 21

Administrativne i stručne poslove u sprovođenju ove Odluke obavljaće Odeljenje za društvene delatnosti.

VII ZAVRŠNE ODREDBE

Član 22

Odeljenje za skupštinske i izvršne poslove gradske uprave vodi evidenciju o dobitnicima javnih priznanja grada Sombora.

Sadržaj evidencije čine podaci o dobitnicima, kao i o oblastima njihovog delovanja koje čine osnov dodele javnog priznanja.

Jedan primerak Odluke o javnom priznanju, uz fotografiju lica koje je dobilo javno priznanje, sa njihovim podacima trajno se čuva u arhivi Gradske uprave.

Član 23

Danom stupanja na snagu ove Odluke prestaje da važi Odluka o povelji i nagradi grada Sombora ("Sl. list grada Sombora", br. 6/10).

Član 24

Ova Odluka stupa na snagu osmog dana od dana objavljivanja u "Službenom listu grada Sombora".