Član 1

Ustanovljava se nagrada za humano delo na teritoriji grada Užica (u daljem tekstu: nagrada) kao poseban oblik priznanja grada Užica.

Član 2

Nagrada se dodeljuje pojedincu ili grupi za zajedničko delo.

Član 3

Odluku o dodeli nagrade donosi Gradsko veće na predlog Komisije za dodelu javnih priznanja i nagrada.

Nagrada se sastoji od pisanog dokumenta - "Nagrada za humano delo" i novčanog iznosa u visini Nagrade grada Užica.

Novčani iznos nagrade obezbeđuje se u budžetu grada.

Član 4

Odluka stupa na snagu danom objavljivanja u "Službenom listu grada Užica".