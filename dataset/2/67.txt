Sadržina odluke

Član 1

Ovom odlukom uređuju se namena, sadržina, izgled i upotreba pečata koje u vršenju poslova iz svog delokruga koriste organi grada Sombora (u daljem tekstu: Organi).

Namena pečata

Član 2

Pečatom se potvrđuje autentičnost javne isprave i drugog akta kojim Organi odlučuju ili službeno opšte sa drugim organima, pravnim licima i građanima.

Sadržina pečata

Član 3

Pečat sadrži grb Republike Srbije oko koga je ispisan tekst: Republika Srbija, Autonomna Pokrajina Vojvodina, Grad Sombor, naziv Organa (može da sadrži i naziv unutrašnje organizacione jedinice u Organu), Sombor.

Jezik na kome se ispisuje tekst pečata

Član 4

Tekst pečata ispisuje se na srpskom jeziku ćiriličnim i latiničnim pismom i na mađarskom jeziku i pismu.

Način ispisivanja teksta pečata

Član 5

Tekst pečata ispisuje se u koncentričnim krugovima oko grba Republike Srbije.

U spoljnom krugu pečata ispisuje se naziv Republike Srbije.

Naziv autonomne pokrajine ispisuje se u prvom sledećem krugu ispod naziva Republike Srbije.

U sledećem unutrašnjem krugu ispisuje se naziv Organa.

Naziv unutrašnje organizacione jedinice Organa ispisuje se u sledećem unutrašnjem krugu.

Sombor ispisuje se u dnu pečata.

Tekst pečata na srpskom jeziku ćiriličkim pismom ispisuje se u svakom krugu iznad grba Republike Srbije, a tekst na jeziku i pismu nacionalnih manjina ispisuje se u nastavku svakog kruga, zaključno sa sedištem organa.

Oblik i veličina pečata

Član 6

Pečat je okruglog oblika.

Prečnik pečata iznosi 60 mm.

Broj pečata

Član 7

Organi imaju jedan pečat za otisak hemijskom bojom, a mogu imati i po jedan pečat za otisak u pečatnom vosku i za suvi otisak.

Organi mogu imati više primeraka pečata koji moraju biti istovetni po sadržini i veličini. Svaki primerak obeležava se rednim brojem, rimskom cifrom koja se stavlja između grba Republike Srbije i sedišta organa - Sombor.

Broj primeraka pečata određuje rukovodilac Organa.

Mali pečat

Član 8

Organ može imati pečat manjeg prečnika (u daljem tekstu: mali pečat).

Sadržina malog pečata je ista i ispisana je na isti način kao i sadržina pečata iz članova 3. i 5. ove odluke. U malom pečatu naziv organa može biti skraćen - tako da se iz skraćenog teksta nedvosmisleno vidi čiji je pečat.

Mali pečat koristi se u slučajevima u kojima je njegova upotreba podesnija.

Prečnik malog pečata je 28 mm.

O potrebi postojanja malog pečata, kao i o broju primeraka, veličini, skraćenom nazivu organa i upotrebi malog pečata odlučuje rukovodilac Organa.

Čuvanje pečata i rukovanje pečatom

Član 9

Pečat se čuva i njime se rukuje u službenim prostorijama Organa, a izuzetno se može koristiti i van službenih prostorija kada treba izvršiti službene radnje van tih prostorija.

Rukovodilac Organa odlučuje kome će poveriti čuvanje i upotrebu pečata.

Lice kome je pečat poveren na čuvanje dužno je da pečat čuva na način kojim se onemogućava neovlašćeno korišćenje pečata.

Saglasnost na sadržinu i izgled pečata i poveravanje poslova državne uprave

Član 10

Pre nabavke pečata organ je dužan da pribavi saglasnost na sadržinu i izgled pečata od organa Autonomne Pokrajine Vojvodine nadležnog za poslove uprave.

Član 11

Stavljanje pečata van upotrebe

Član 12

Pečat koji je postao neodgovarajući zbog istrošenosti, oštećenja, promene uređenja Organa (promena naziva, sedišta, preuzimanje nadležnosti, prestanak rada organa ili ukidanje unutrašnje jedinice i sl.), odnosno prestanka vršenja javnih ovlašćenja, stavlja se van upotrebe i mora se uništiti.

Uništavanje pečata vrši komisija Organa i o izvršenom uništenju pečata obaveštava nadležni organ autonomne pokrajine.

Nestanak ili gubitak pečata

Član 13

Nestanak ili gubitak pečata bez odlaganja se prijavljuje nadležnom organu autonomne pokrajine i oglašava nevažećim u službenom glasilu autonomne pokrajine.

Nestalim ili izgubljenim pečatom smatra se pečat koji nije dostupan licu ovlašćenom za čuvanje i upotrebu pečata.

Zahtev za oglašavanje pečata nevažećim podnosi se službenom glasilu autonomne pokrajine, u roku od tri dana od saznanja za nestanak ili gubitak pečata.

Pečat se smatra nevažećim od dana prijavljivanja njegovog nestanka ili gubitka.

U slučaju kasnijeg pronalaska pečat će se uništiti.

Dostavljanje podataka o izrađenim pečatima

Član 14

Otisak izrađenog pečata, podatke o broju primeraka pečata i datumu početka njegove upotrebe organ je dužan da dostavi nadležnom organu autonomne pokrajine, u roku od deset dana od dana izrade pečata.

Stupanje na snagu

Član 15

Ova Odluka stupa na snagu osmog dana od dana objavljivanja u Službenom listu grada Sombora.