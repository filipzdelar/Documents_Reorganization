I OPŠTE ODREDBE

Član 1

Ovom odlukom ustanovljavaju se javna priznanja i nagrade grada Kraljeva za rad i dela koja zaslužuju posebno priznanje i isticanje i uređuje se postupak za njihovo dodeljivanje.

Član 2

Priznanja grada Kraljeva su:

- Zvanje počasnog građanina grada Kraljeva;

- Zahvalnica;

- Diploma zaslužnog građanina grada Kraljeva;

- Diploma zaslužnog preduzeća i ustanove grada Kraljeva;

- Diploma zaslužne sportske organizacije grada Kraljeva;

- Diploma zaslužne obrazovne ustanove grada Kraljeva;

- Diploma zaslužnog udruženja građana grada Kraljeva.

Član 3

Nagrade grada Kraljeva su Oktobarska nagrada grada Kraljeva i Specijalna nagrada za podsticaj humanosti "Milomir Glavčić".

Član 4

Priznanja grada Kraljeva dodeljuju se građanima-državljanima Republike Srbije, sa prebivalištem na teritoriji grada Kraljeva, odnosno preduzećima, ustanovama, udruženjima građana, organizacijama ili mesnim zajednicama sa sedištem na teritoriji grada Kraljeva.

Priznanja grada Kraljeva mogu se dodeliti i građanima-državljanima Republike Srbije, čije prebivalište nije na teritoriji grada Kraljeva, odnosno preduzećima, ustanovama, udruženjima građana, organizacijama ili mesnim zajednicama čije sedište nije na teritoriji grada Kraljeva, a mogu se dodeliti i stranim državljanima, stranim preduzećima, ustanovama, udruženjima građana ili organizacijama.

Član 5

Oktobarska nagrada dodeljuje se licu koje je državljanin Republike Srbije.

II PRIZNANJE KOJE DODELJUJE GRADONAČELNIK GRADA KRALJEVA

Član 6

Gradonačelnik grada Kraljeva dodeljuje Zahvalnicu, u prigodnim prilikama.

Član 7

(Brisano)

Zahvalnica

Član 8

Zahvalnica se dodeljuje građanima, preduzećima, ustanovama, udruženjima, organizacijama ili mesnim zajednicama za postignute primerene rezultate i uspehe u radu, učinjene podvige, humana dela, kao i za ispoljenu hrabrost i požrtvovanje u spasavanju života ljudi i materijalnih dobara.

Zahvalnica se može dodeliti i građanima, preduzećima, ustanovama, udruženjima ili organizacijama čije prebivalište, odnosno sedište nije na teritoriji grada Kraljeva, kao i stranima državljanima, preduzećima, ustanovama, udruženjima ili organizacijama, ukoliko je njihov doprinos od značaja za grad Kraljevo.

Uručivanje priznanja

Član 9

Zahvalnicu uručuje gradonačelnik, odnosno zamenik gradonačelnika grada Kraljeva.

U slučaju odsutnosti lica iz prethodnog stava, zahvalnicu može da uruči i lice koje posebnim rešenjem odredi gradonačelnik grada Kraljeva.

III PRIZNANJA I NAGRADE KOJE DODELJUJE SKUPŠTINA GRADA KRALJEVA

Član 10

Odluku o dodeli Zvanja počasnog građanina grada Kraljeva, Diplome zaslužnog građanina grada Kraljeva, Diplome zaslužnog preduzeća i ustanove grada Kraljeva, Diplome zaslužne sportske organizacije grada Kraljeva, Diplome zaslužne obrazovne ustanove grada Kraljeva, Diplome zaslužnog udruženja građana grada Kraljeva, Oktobarske nagrade grada Kraljeva i Specijalne nagrade za podsticaj humanosti "Milomir Glavčić", donosi Skupština grada Kraljeva povodom 7. oktobra - Kraljevdana, Dana grada Kraljeva.

Član10a

Zvanje počasnog građanina grada Kraljeva dodeljuje se kao najviša lična počast i izraz posebnog poštovanja građanima koji uživaju veliki ugled u zemlji i svetu.

Zvanje počasnog građanina može se dodeliti i stranim državljanima.

Počasnom građaninu grada Kraljeva izdaje se Povelja.

Diploma zaslužnog građanina grada Kraljeva

Član 11

Diploma zaslužnog građanina grada Kraljeva dodeljuje se građanima za učinjena humana dela, za dugogodišnji rad i ostvarenja u oblasti naučno-istraživačkog rada, zdravstva, prosvete, kulture, umetnosti, fizičke kulture, sporta, proizvodnog rada i drugim oblastima društvenog i javnog života, koji su od izuzetnog značaja i predstavljaju trajnu vrednost za grad Kraljevo, a naročito imajući u vidu već dodeljena priznanja od strane profesionalnih ili granskih udruženja.

Diploma zaslužnog građanina grada Kraljeva može se dodeliti i građanima koji nemaju prebivalište na teritoriji grada Kraljeva, kao i stranim državljanima, ukoliko je njihov doprinos od izuzetnog značaja za grad Kraljevo.

Diploma zaslužnog preduzeća ili ustanove grada Kraljeva

Član 12

Diploma zaslužnog preduzeća ili ustanove grada Kraljeva dodeljuje se preduzećima ili ustanovama za dugogodišnji rad i izuzetne uspehe u privredi, nauci, zdravstvu, prosveti, kulturi, umetnosti, fizičkoj kulturi, sportu, i drugim oblastima, kao i za druge uspehe, kojima se doprinosi razvoju i afirmaciji grada Kraljeva, a naročito imajući u vidu već dodeljena priznanja od strane profesionalnih ili granskih udruženja.

Diploma zaslužnog preduzeća ili ustanove grada Kraljeva može se dodeliti preduzećima ili ustanovama čije sedište nije na teritoriji grada Kraljeva, kao i stranim preduzećima i ustanovama, ako je njihov doprinos od izuzetnog značaja za grad Kraljevo.

Diploma zaslužne sportske organizacije grada Kraljeva

Član 13

Diploma zaslužne sportske organizacije grada Kraljeva dodeljuje se sportskim organizacijama za dugogodišnji rad i izuzetne uspehe u sportu i fizičkoj kulturi kojima se doprinosi razvoju i afirmaciji grada Kraljeva, a naročito imajući u vidu već dodeljena priznanja od strane profesionalnih ili granskih udruženja.

Diploma zaslužne sportske organizacije grada Kraljeva može se dodeliti sportskim organizacijama čije sedište nije na teritoriji grada Kraljeva, kao i stranim sportskim organizacijama, ako je njihov doprinos od izuzetnog značaja za grad Kraljevo.

Diploma zaslužne obrazovne ustanove grada Kraljeva

Član 14

Diploma zaslužne obrazovne ustanove grada Kraljeva dodeljuje se za dugogodišnji rad i izuzetne uspehe u oblasti prosvete kojima se doprinosi razvoju i afirmaciji grada Kraljeva, a naročito imajući u vidu već dodeljena priznanja od strane profesionalnih ili granskih udruženja.

Diploma zaslužne obrazovne ustanove grada Kraljeva može se dodeliti obrazovnim ustanovama čije sedište nije na teritoriji grada Kraljeva, kao i stranim obrazovnim ustanovama, ako je njihov doprinos od izuzetnog značaja za grad Kraljevo.

Diploma zaslužnog udruženja građana grada Kraljeva

Član 15

Diploma zaslužnog udruženja građana grada Kraljeva dodeljuje se udruženjima građana za humana dela, dugogodišnji rad i izuzetne uspehe u privredi, nauci, zdravstvu, prosveti, kulturi, umetnosti, fizičkoj kulturi, sportu, i drugim oblastima, kao i za druge uspehe, kojima se doprinosi razvoju i afirmaciji grada Kraljeva, a naročito imajući u vidu već dodeljena priznanja od strane profesionalnih ili granskih udruženja.

Diploma zaslužnog udruženja građana grada Kraljeva može se dodeliti udruženjima građana čije sedište nije na teritoriji grada Kraljeva, kao i stranim udruženjima građana, ako je njihov doprinos od izuzetnog značaja za grad Kraljevo.

Oktobarska nagrada grada Kraljeva

Član 16

Oktobarska nagrada grada Kraljeva (u daljem tekstu: nagrada), dodeljuje se za delo koje predstavlja najvrednije dostignuće u nauci, kulturi, umetnosti, prosveti, zdravstvu, fizičkoj kulturi, sportu, novinarstvu, proizvodnom radu ili u drugim oblastima privrednog i društvenog života.

Nagrada se može dodeliti i za celokupno stvaralaštvo.

Član 17

Nagrada se dodeljuje pojedincima ili grupama autora za zajedničko ostvarenje.

Nagrada se može dodeliti istom licu samo jednom, izuzev u slučaju zajedničkog dela u kome je to lice jedan od autora.

Nagrada se dodeljuje za delo koje je postalo dostupno javnosti publikovanjem, izlaganjem ili dostupno na drugi način u periodu od 1. avgusta prethodne godine do 31. jula godine u kojoj se nagrada dodeljuje.

Član 18

Pod delom koje je, u smislu člana 17. ove odluke, postalo dostupno javnosti smatra se:

- delo u oblasti naučno-istraživačkog rada i umetnosti objavljeno u roku predviđenom za nagradu;

- ostvarenje u oblasti kulturnog stvaralaštva, angažovanju na širenju i razvoju kulture i razvijanju aktivnosti amaterskih kulturno-umetničkih društava, kao i stvaranje uslova za korišćenje kulturnih dobara;

- delo likovne i primenjene umetnosti, ako je izloženo u galeriji i drugom prostoru namenjenom za izlaganje dela iz ove oblasti;

- osavremenjavanje procesa nastave i objavljivanje stručnih radova sa obrazovno-vaspitnom problematikom;

- uvođenje i primena dostignuća u oblasti medicine i doprinos razvoju i unapređivanju zdravstva;

- rezultati ostvareni u oblasti unapređenja i zaštite životne sredine;

- vrhunski rezultati u oblasti sporta i doprinos razvoju i unapređivanju fizičke kulture i sporta;

- novinarsko dostignuće ostvareno u pisanim ili elektronskim medijima;

- arhitektonsko-građevinski i drugi projekat objekta po kome je, u roku predviđenom za nagradu, objekat završen, kao i urbanistički planovi, ako je njihova realizacija otpočela;

- stvaranje novog proizvoda (pronalazaštvo), razrada i primena tehnoloških dostignuća i drugi vidovi rada koji omogućavaju oplemenjavanje postojećih i uvođenja novih proizvoda, produktivnost i ekonomičnije poslovanje, ako je njihova realizacija otpočela;

- vrhunski rezultat u oblasti poljoprivredne proizvodnje;

- samostalni radovi (diplomski, seminarski i drugi radovi), učenika i studenata koji predstavljaju doprinos određenoj oblasti nauke, originalno tumačenje nekog problema, delimično novo rešenje praktične primene određenih naučnih rezultata, ali i umetničko dostignuće koje se po vrednosti posebno ističe, kao i rezultati učenika i studenata i nagrade osvojene na republičkim i međunarodnim takmičenjima.

U jednoj godini za svaku od navedenih oblasti može se dodeliti najviše po jedna nagrada.

Specijalna nagrada za podsticaj humanosti "Milomir Glavčić"

Član 19

Specijalna nagrada za podsticaj humanosti "Milomir Glavčić" dodeljuje se građanima, preduzećima, ustanovama, udruženjima, organizacijama ili mesnim zajednicama za humana dela i podsticaj humanosti u različitim oblastima društvenog i javnog života.

Specijalna nagrada za podsticaj humanosti "Milomir Glavčić" se može dodeliti i građanima, preduzećima, ustanovama, udruženjima, organizacijama ili mesnim zajednicama čije prebivalište, odnosno sedište nije na teritoriji grada Kraljeva, kao i stranima državljanima, preduzećima, ustanovama, udruženjima ili organizacijama, ukoliko je njihov doprinos od značaja za grad Kraljevo.

Član 20

Nagrada se može dodeliti istom licu samo jednom, izuzev u slučaju zajedničkog delovanja u kome je to lice jedan od učesnika.

Specijalna nagrada za podsticaj humanosti "Milomir Glavčić" dodeljuje se za period od 1. avgusta prethodne godine do 31. jula godine u kojoj se nagrada dodeljuje.

U jednoj godini može se dodeliti najviše jedna nagrada.

Član 21

Oktobarska nagrada i Specijalna nagrada se dodeljuju u vidu diplome.

Uz diplome se dodeljuju i novčani iznosi nagrada.

Sredstva za nagrade i druge troškove obezbeđuju se iz budžeta grada Kraljeva.

Gradsko veće grada Kraljeva svake godine utvrđuje visinu novčanih nagrada u okviru sredstava utvrđenih budžetom grada za tekuću godinu.

Postupak sprovodi Komisija za nagrade i priznanja Skupštine grada Kraljeva.

Član 22

Komisija za nagrade i priznanja Skupštine grada Kraljeva (u daljem tekstu: Komisija), blagovremeno, na svojoj sednici, utvrđuje tekst javnog poziva, u kom određuje rok za podnošenje predloga za dodelu priznanja i nagrada.

Javni poziv se obavezno objavljuje u sredstvima javnog informisanja i na zvaničnoj internet stranici grada Kraljeva.

Član 23

Predloge za dodelu Zvanja počasnog građanina grada Kraljeva, Diplome zaslužnog građanina, Diplome zaslužnog preduzeća i ustanove, Diplome zaslužne sportske organizacije, Diplome zaslužne obrazovne ustanove, Diplome zaslužnog udruženja građana, Oktobarske nagrade i Specijalne nagrade za podsticaj humanosti "Milomir Glavčić" mogu da podnesu građani, preduzeća, ustanove, organizacije, mesne zajednice, društva, stručna i druga udruženja.

Predlozi se podnose Komisiji u pisanoj formi i moraju biti obrazloženi.

Član 24

Komisija, po isteku roka za predlaganje, razmatra svaki prispeli predlog za dodelu priznanja i nagrada koji je blagovremen i potpun.

Ukoliko predlog nije dovoljno obrazložen i dokumentovan, Komisija će zatražiti od predlagača da u određenom roku dopuni predlog u pisanoj formi, a ako to ne učini u ostavljenom roku, smatraće se kao da predlog nije ni podnet.

Član Komisije predložen za priznanje, odnosno nagradu, ne učestvuje u radu Komisije prilikom odlučivanja o dodeli priznanja, odnosno nagrade za koju je predložen.

Član 25

Ako Komisija smatra da u određenoj oblasti nema dela koja ispunjavaju uslove dodeljivanja priznanja i nagrada, može predložiti Skupštini grada Kraljeva da priznanje ne dodeli.

Član 26

U pripremanju predloga odluka za dodeljivanje priznanja i nagrada Komisija može formirati stručna tela od svojih članova, kao i od istaknutih naučnih, kulturnih i javnih radnika i zatražiti mišljenje od odgovarajućih institucija i pojedinih stručnjaka za određenu oblast o delu predloženom za nagrade i priznanja.

Član 27

Nakon razmatranja blagovremenih i potpunih predloga Komisija utvrđuje predloge odluka o dodeljivanju priznanja i nagrada, pojedinačno za svakog predloženog kandidata, uz obrazloženje čime se rukovodila kod utvrđenog predloga odluka.

Komisija nakon utvrđivanja predloga odluke o dodeli Zvanja počasnog građanina grada Kraljeva, navedeni predlog odluke dostavlja na prethodnu saglasnost ministarstvu nadležnom za poslove lokalne samouprave.

Na ovako utvrđene predloge odluka nije moguće podnošenje amandmana.

Donošenje odluka o dodeli priznanja i nagrada

Član 28

Odluke o dodeli priznanja i nagrada donosi Skupština grada Kraljeva na redovnoj sednici.

O svakom predlogu odluke koji je utvrdila Komisija, odbornici se posebno izjašnjavaju, a odluke donose većinom glasova prisutnih odbornika.

Odluke o dodeli priznanja i nagrada objavljuju se u "Službenom listu grada Kraljeva".

Uručivanje priznanja i nagrada

Član 29

Povelja počasnom građaninu grada Kraljeva, Diploma zaslužnog građanina grada Kraljeva, Diploma zaslužnog preduzeća i ustanove grada Kraljeva, Diploma zaslužne sportske organizacije grada Kraljeva, Diploma zaslužne obrazovne ustanove grada Kraljeva, Diploma zaslužnog udruženja građana grada Kraljeva, Oktobarska nagrada i Specijalna nagrada za podsticaj humanosti "Milomir Glavčić" uručuju se 7. oktobra na svečanoj sednici Skupštine grada Kraljeva, koja se održava povodom Kraljevdana - Dana grada Kraljeva.

Priznanja i nagrade grada Kraljeva uručuju se na svečani način.

Član 30

Priznanja i nagrade iz člana 29. ove odluke uručuje predsednik Skupštine grada Kraljeva, odnosno njegov zamenik.

U slučaju odsutnosti lica iz prethodnog stava, priznanja i nagrade može da uruči i lice koje posebnim ovlašćenjem odredi predsednik Skupštine grada Kraljeva.

Član 31

Pre uručenja priznanja i nagrada čitaju se odluke o dodeli priznanja i nagrada grada Kraljeva.

Ako se određeno priznanje i nagrade uručuju stranom državljaninu, odluka Skupštine grada Kraljeva prevodi se na odgovarajući strani jezik i pre uručenja čita na tom jeziku.

Član 32

Svečanom uručenju priznanja i nagrada grada Kraljeva obezbediće se odgovarajući publicitet preko sredstava javnog informisanja.

Oduzimanje priznanja

Član 33

Skupština grada Kraljeva, na predlog Komisije, može doneti odluku o oduzimanju dodeljenog priznanja i nagrade u sledećim slučajevima:

- ako je lice koje je dobilo priznanje pravosnažnom odlukom osuđeno za krivično delo koje ga čini nedostojnim nosioca priznanja i

- ako lice koje je dobilo priznanje svojim nedoličnim ponašanjem obezvređuje institut priznanja i ruši ugled Skupštine grada Kraljeva.

Vođenje evidencije o dodeljenim priznanjima i nagradama, administrativni i drugi poslovi

Član 34

O dodeljenim priznanjima i nagradama koje se dobitnicima priznanja i nagrada mogu izdavati potvrde.

Član 35

Administrativne i druge poslove za potrebe Komisije, kao i evidenciju o dodeljenim priznanjima i nagradama, vodi Odeljenje za poslove organa Grada Gradske uprave grada Kraljeva.

IV PRELAZNE I ZAVRŠNE ODREDBE

Član 36

Danom stupanja na snagu ove odluke prestaje da važi Odluka o priznanjima i nagradi grada Kraljeva ("Službeni list grada Kraljeva", broj 17/2009).

Član 37

Ova odluka stupa na snagu osmog dana od dana objavljivanja u "Službenom listu grada Kraljeva".