Član 1

Ovom Odlukom se uređuju vrste, uslovi, postupak i druga pitanja u vezi dodele nagrada i drugih javnih priznanja Grada.

Član 2

Grad Čačak dodeljuje Decembarsku nagradu grada, zvanje Počasni građanin grada Čačka i Zahvalnicu grada Čačka.

Član 3

Povodom Dana grada 18. decembra, dodeljuje se Decembarska nagrada grada i zvanje Počasnog građanina grada Čačka.

Gradonačelnik grada Čačka može kao posebno priznanje u prigodnim prilikama u toku godine dodeliti Zahvalnicu grada Čačka.

Član 4

Odluku o dodeli Decembarske nagrade grada i zvanja Počasnog građanina grada Čačka donosi Skupština grada.

Decembarska nagrada grada i zvanje Počasnog građanina se uručuju na svečanoj sednici Skupštine.

Nagrada i zvanje Počasnog građanina se mogu dodeliti istom dobitniku samo jednom.

Član 5

Decembarska nagrada grada se uručuje u obliku povelje.

Izgled i sadržinu povelje utvrđuje Komisija za obeležavanje praznika i dodelu priznanja.

Član 6

Decembarska nagrada grada se dodeljuje za naročito isticanje u privrednom, naučnom, kulturnom, prosvetnom, zdravstvenom, sportskom i drugim oblicima stvaralaštva.

Član 7

Nagradu može dobiti fizičko ili pravno lice koje ima prebivalište, odnosno sedište, na teritoriji grada Čačka.

Član 8

Komisija za obeležavanje praznika i dodelu priznanja objavljuje javni poziv za dostavljanje predloga za dodelu Decembarske nagrade u lokalnim sredstvima javnog informisanja.

Javni poziv iz prethodnog stava sadrži uslove, način i rokove dostavljanja predloga.

Član 9

Predlog za dodelu Decembarske nagrade grada mogu podneti preduzeća, ustanove, druge organizacije i organi, grupe građana i pojedinci.

Predlog iz prethodnog stava mora biti obrazložen.

Član 10

Predlog odluke za dodelu Decembarske nagrade grada u skladu sa ovom Odlukom utvrđuje Komisija za obeležavanje praznika i dodelu priznanja uz obrazloženje za svakog kandidata.

Utvrđeni predlog Komisije nije moguće menjati.

O svakom kandidatu koga je predložila Komisija Skupština se posebno izjašnjava a odluka se donosi većinom glasova prisutnih odbornika.

Član 11

Broj dobitnika Decembarske nagrade grada utvrđuje Komisija.

U jednoj godini mogu se dodeliti najviše tri nagrade.

Član 12

Zvanje Počasni građanin grada Čačka se može dodeliti pojedincu koji svojim radom, naučnim, umetničkim, političkim, humanitarnim i drugim delovanjem doprinosi razvoju i afirmaciji Grada u zemlji i inostranstvu.

Član 13

Zvanje Počasni građanin grada Čačka može se dodeliti građanima Republike Srbije, kao i stranim državljanima, koji nemaju prebivalište na teritoriji grada Čačka.

Član 14

Licu kome je dodeljeno zvanje Počasni građanin grada Čačka uručuje se plaketa sa grbom grada Čačka.

Izgled i sadržinu plakete utvrđuje Komisija za obeležavanje praznika i dodelu priznanja.

Član 15

Predlog za zvanje Počasnog građanina grada Čačka utvrđuje Komisija za obeležavanje praznika i dodelu priznanja.

Inicijativu za zvanje Počasnog građanina grada Čačka sa obrazloženjem mogu podneti preduzeća, ustanove, druge organizacije i organi, grupe građana i pojedinci do 1. novembra tekuće godine.

Član 16

Komisija utvrđuje predloge odluka o dodeli nagrade i zvanja Počasnog građanina grada Čačka većinom glasova od ukupnog broja članova Komisije.

Ukoliko Komisija ustanovi da niko od predloženih kandidata ne ispunjava uslove, nagrada odnosno zvanje Počasnog građanina se neće dodeliti.

Član 17

Zahvalnica se može dodeliti domaćem ili stranom fizičkom ili pravnom licu kao i domaćim ili stranim gradovima ili institucijama za izuzetan doprinos u afirmaciji i promovisanju grada Čačka, kao i pružanju pomoći gradu ili građanima Čačka.

Član 18

Odluku o dodeljivanju Zahvalnice grada Čačka donosi Gradonačelnik.

Zahvalnicu grada Čačka uručuje Gradonačelnik grada Čačka.

Član 19

(Brisan)

Član 20

Stupanjem na snagu ove Odluke prestaje da važi Odluka o danu opštine i gradskoj slavi ("Sl. list opštine Čačak" br. 1/99).

Član 21

Ova Odluka stupa na snagu osmog dana od dana objavljivanja u "Službenom listu grada Čačka".

 

Samostalni član Odluke o izmenama
Odluke o nagradama i priznanjima grada Čačka

("Sl. list grada Čačka", br. 19/2017)

Član 5

Ova Odluka stupa na snagu narednog dana od dana objavljivanja u "Službenom listu grada Čačka".