I OPŠTE ODREDBE

Član 1

Ovom Odlukom se ustanovljavaju nagrade i javna priznanja grada Novog Pazara, koje dodeljuje grad Novi Pazar, organizacijama i građanima za značajna ostvarenja u proizvodnji, nauci, umetnosti i drugim društvenim oblastima kao i uslovi, kriterijumi i način njihovog dodeljivanja, kao i prava koja njihovim nosiocima pripadaju.

Član 2

Grad Novi Pazar dodeljuje javna priznanja, nagrade i zvanje počasnog građanina.

Kao javno priznanje grada Novog Pazara ustanovljava se:

- zvanje ZASLUŽNI GRAĐANIN GRADA NOVOG PAZARA i PLAKETA GRADA NOVOG PAZARA.

Kao nagrada grada Novog Pazara ustanovljava se:

- POVELJA GRADA NOVOG PAZARA.

Grad Novi Pazar dodeljuje i zvanje:

- POČASNI GRAĐANIN GRADA NOVOG PAZARA.

Član 3

Povodom Dana Grada Novog Pazara mogu se dodeliti:

- zvanje POČASNI GRAĐANIN GRADA NOVOG PAZARA.

- zvanje ZASLUŽNI GRAĐANIN GRADA NOVOG PAZARA.

- GODIŠNJA NAGRADA GRADA NOVOG PAZARA.

Član 4

Za izuzetan doprinos u afirmaciji i promovisanju grada Novog Pazara u zemlji i inostranstvu gradonačelnik grada Novog Pazara može, u toku godine dodeliti javno priznanje:

- PLAKETA GRADA NOVOG PAZARA.

Član 5

Zvanje POČASNI GRAĐANIN GRADA NOVOG PAZARA, zvanje ZASLUŽNI GRAĐANIN GRADA NOVOG PAZARA i PLAKETA GRADA NOVOG PAZARA mogu se dodeliti samo jedanput istom licu.

GODIŠNJA NAGRADA GRADA NOVOG PAZARA može se dodeliti istom licu više puta.

Član 6

Zvanje POČASNI GRAĐANIN GRADA NOVOG PAZARA i ZASLUŽNI GRAĐANIN GRADA NOVOG PAZARA mogu se dodeliti, za najviše 3 lica, u godini u kojoj se dodeljuju ova zvanja, odnosno zvanje.

II POČASNI GRAĐANIN GRADA NOVOG PAZARA

Član 7

Zvanje POČASNI GRAĐANIN GRADA NOVOG PAZARA može se dodeliti - za izuzetan i višegodišnji doprinos:

- razvoju grada u različitim oblastima,

- afirmaciji grada u zemlji i inostranstvu,

- afirmaciji vrednosti koje su utemeljene u istorijskoj ulozi Novog Pazara i njegovom značenju u razvoju moderne države i doprinosu dobrobiti njegovih žitelja,

- u pružanju humanitarne pomoći i donacija,

- razvoju demokratije,

- zaštiti i unapređenju ljudskih prava i sloboda,

- unapređenju i razvoju lokalne samouprave.

Član 8

Zvanje POČASNI GRAĐANIN GRADA NOVOG PAZARA može se dodeliti domaćem ili stranom državljaninu, koji nema prebivalište na teritoriji grada Novog Pazara.

Član 9

Priznanje iz člana 7. ove Odluke, dodeljuje se u obliku Medalje i Diplome.

Medalja sadrži: na prednjoj strani (lice medalje), reljefnu interpretaciju grba grada, okruglog oblika, prečnika 50 mm, u galvanizaciji pozlate, a na zadnjoj strani, (naličje medalje) ugraviran tekst: Počasni građanin, ime i prezime dobitnika i godina uručenja.

Medalja se uručuje u kutiji, kraljevsko crvene boje.

Diploma je dimenzija B3, sa utisnutim grbom grada, izrađena od kvalitetnog grafičkog ili akvarel 250-300 gr papira, na kojoj je kaligrafskim slovima ispisan tekst:

NOVI PAZAR

grad otvorenog srca
prima u svoje okrilje
kao

POČASNOG GRAĐANINA GRADA NOVOG PAZARA

___________________________________________________
(ime i prezime)

u znak zahvalnosti za neizmeran doprinos njegovoj dobrobiti, razvoju i afirmaciji

U Novom Pazaru,

 

GRADONAČELNIK GRADA
NOVOG PAZARA

 

 

U donjem levom uglu Diplome, na pečatnom vosku sa jemstvenikom, utisnut je pečat grba grada Novog Pazara.

U donjem desnom uglu Diplome, kaliografskim slovima ispisan je tekst: gradonačelnik grada Novog Pazara, ispod koga je gradonačelnik grada svojeručno potpisan.

Diploma se uručuje u futroli, kraljevsko crvene boje, na kojoj je zlatotiskom ispisan tekst: grad Novi Pazar.

Član 10

Predlog za dodelu priznanja iz člana 7. ove Odluke, utvrđuje Odbor za obeležavanje praznika, dodelu priznanja, saradnju i udruživanje sa drugim gradovima i opštinama, na inicijativu Odbora za obeležavanje Dana grada.

Odbor za obeležavanje Dana grada obrazuje se aktom gradonačelnika grada.

Odluku o dodeli zvanja POČASNI GRAĐANIN GRADA NOVOG PAZARA donosi Skupština grada Novog Pazara.

Član 11

Zvanje POČASNI GRAĐANIN GRADA NOVOG PAZARA, uručuje gradonačelnik grada ili predsednik Skupštine grada na svečanoj sednici Skupštine grada povodom Dana grada.

Član 12

POČASNI GRAĐANIN GRADA NOVOG PAZARA, se obavezno poziva na svečane sednice grada Novog Pazara povodom Dana grada, na praznike i jubileje koje grad obeležava, kao i svečanosti čiji je organizator grad.

III ZASLUŽNI GRAĐANIN GRADA NOVOG PAZARA

Član 13

Zvanje ZASLUŽNI GRAĐANIN GRADA NOVOG PAZARA, može se dodeliti

- za izuzetan i višegodišnji doprinos:

- razvoju grada u različitim oblastima,

- afirmaciji grada u zemlji i inostranstvu,

- afirmaciji vrednosti koje su utemeljene u istorijskoj ulozi Novog Pazara i njegovom značenju u razvoju moderne države i doprinosu dobrobiti njegovih žitelja,

- u pružanju humanitarne pomoći i donacija,

- razvoju demokratije,

- zaštiti i unapređenju ljudskih prava i sloboda,

- unapređenju i razvoju lokalne samouprave.

Član 14

Zvanje ZASLUŽNI GRAĐANIN GRADA NOVOG PAZARA, može se dodeliti licu koje ima prebivalište na teritoriji Novog Pazara.

Član 15

Priznanje iz člana 13. ove Odluke, dodeljuje se u obliku Medalje i Diplome.

Medalja sadrži: na prednjoj strani (lice medalje), reljefnu interpretaciju grba grada, okruglog oblika, prečnika 50 mm, u galvanizaciji pozlate, a na zadnjoj strani, (naličje medalje) ugraviran tekst: Počasni građanin, ime i prezime dobitnika i godina uručenja.

Medalja se uručuje u kutiji, kraljevsko crvene boje.

Diploma je dimenzija B3, sa utisnutim grbom grada, izrađena od kvalitetnog grafičkog ili akvarel 250-300 gr papira, na kojoj je kaligrafskim slovima ispisan tekst:

Novi Pazar
grad koji uvažava i poštuje izuzetan

Doprinos
vrednosti svoga imena

U znak zahvalnosti
proglašava

___________________________________________________
(ime i prezime)

ZASLUŽNIM GRAĐANINOM GRADA NOVOG PAZARA

U Novom Pazaru,

 

GRADONAČELNIK GRADA
NOVOG PAZARA

 

 

U donjem levom uglu Diplome, na pečatnom vosku sa jemstvenikom, utisnut je pečat grba grada Novog Pazara.

U donjem desnom uglu Diplome, kaliografskim slovima ispisan je tekst: gradonačelnik grada Novog Pazara, ispod koga je gradonačelnik grada svojeručno potpisan.

Diploma se uručuje u futroli, kraljevsko crvene boje, na kojoj je zlatotiskom ispisan tekst: grad Novi Pazar.

Član 16

Predlog za dodelu priznanja iz člana 13. ove Odluke, utvrđuje Odbor za obeležavanje praznika, dodelu priznanja, saradnju i udruživanje sa drugim gradovima i opštinama, na inicijativu Odbora za obeležavanje Dana grada.

Odbor za obeležavanje Dana grada obrazuje se aktom gradonačelnika grada.

Odluku o dodeli zvanja ZASLUŽNI GRAĐANIN GRADA NOVOG PAZARA donosi Skupština grada Novog Pazara.

Član 17

Zvanje ZASLUŽNI GRAĐANIN GRADA NOVOG PAZARA, uručuje gradonačelnik grada ili predsednik Skupštine grada na svečanoj sednici Skupštine grada povodom Dana grada.

Član 18

ZASLUŽNI GRAĐANIN GRADA NOVOG PAZARA, se obavezno poziva na svečane sednice grada Novog Pazara povodom Dana grada, na praznike i jubileje koje grad obeležava, kao i svečanosti čiji je organizator grad.

IV POVELJA GRADA NOVOG PAZARA

Član 19

Godišnja nagrada grada Novog Pazara (u daljem tekstu: Nagrada) dodeljuje se za delo koje predstavlja najvrednije dostignuće u Novom Pazaru oblastima: privrede, privatnog preduzetništva, pronalazaštva, umetnosti, nauke, medicine, arhitekture i urbanizma, novinarstva, obrazovanja, sporta, doprinosa u akcijama humanitarnog karaktera i drugim oblastima značajnim za grad.

Član 20

Nagradu može dobiti fizičko ili pravno lice koje ima prebivalište, odnosno sedište, na teritoriji grada Novog Pazara.

Nagrada može biti dodeljena pojedincu ili grupi za zajedničko delo.

Član 21

Nagrada se dodeljuje za dostignuće iz člana 19. ove Odluke, za period od 1. januara do 31. decembra godine, koja prethodi godini u kojoj se nagrada dodeljuje.

Član 22

Pod dostignućem koje je postalo dostupno javnosti, u periodu iz člana 19. ove Odluke, podrazumeva se:

1. u oblasti umetnosti:

- publikovano delo književnog i prevodnog stvaralaštva,

- izvedeno, odnosno prikazano delo na pozorišnoj sceni i drugom prostoru namenjenom za izvođenje dela ove vrste,

- prikazano filmsko i radio-televizijsko ostvarenje u bioskopskoj dvorani ili emitovano na radio-televizijskom programu,

- izloženo, odnosno prezentirano delo u galeriji i drugom prostoru namenjenom za prezentaciju dela likovnog i primenjenog stvaralaštva, vizuelnih i proširenih medija,

- izvedeno muzičko ili muzičko-scensko delo u koncertnoj ili pozorišnoj dvorani i drugom prostoru namenjenom za izvođenje dela ove vrste;

2. u oblasti nauke:

- naučno ostvarenje, odnosno rad u društvenim, humanističkim, prirodnim i tehničkim naukama, koji kao rezultat ima nova saznanja i sintezu postojećih u cilju njihove primene, koji predstavlja doprinos razvoju naučne oblasti ili razvoju novih proizvoda, metoda ili tehnoloških postupaka, pod uslovom da je objavljeno u naučnoj i stručnoj literaturi, ogovarajućim naučnim časopisima ili drugim načinom javnog saopštavanja,

- pronalazak koji je u smislu odredaba Zakona o patentima, priznat i upisan u zakonom utvrđen registar;

3. u oblasti medicine:

- rad ili rezultati izuzetne vrednosti pojedinaca kojima je dao značajan doprinos razvoju i unapređenju medicine,

- izuzetno delo koje predstavlja doprinos razvoju medicine, pod uslovom da je objavljeno u naučnoj i stručnoj literaturi, odgovarajućim naučnim časopisima ili drugim načinom javnog saopštenja;

4. u oblasti arhitekture i urbanizma:

- realizovano delo iz oblasti arhitekture, urbanizma ili rekonstrukcije objekta,

- urbanistički plan za koncepciju i metodologiju, pod uslovom da su usvojeni i da je njihova realizacija otpočela;

5. u oblasti novinarstva:

- vrhunsko ostvarenje pojedinaca u novinarstvu ili publicistici, za seriju ili više natpisa, televizijskih ili radijskih priloga ili emisija i publicističkih radova o temama od značaja za politički, kulturni, privredni i ukupni društveni život u Novom Pazaru,

- kreativni doprinos uređivanju rubrike, emisije i programa kojima se značajno doprinosi informisanju građana Novog Pazara;

6. u oblasti obrazovanja:

- izuzetno delo i rezultat vaspitno-obrazovnog ili instruktivnog rada i rezultat u organizaciji i razvoju školstva Novog Pazara u celini, ili u pojedinim njegovim oblastima;

7. u oblasti sporta:

- rad ili rezultat izuzetne vrednosti pojedinaca kojima je dao značajan doprinos afirmaciji, razvoju i unapređenju sporta (vrhunski sportisti - članovi sportskih organizacija iz Novog Pazara odnosno samostalni profesionalni sportisti, za izvanredne rezultate u određenoj grani sporta; sportski stručnjaci, stručnjaci u sportu i naučni radnici za naročito vredne doprinose unapređenju oblasti sporta, sportski radnici za doprinos organizovanju, razvoju i unapređenju sporta);

8. u oblasti privrede ili privatnog preduzetništva:

- rad ili rezultati izuzetne vrednosti pojedinaca ili pravnog lica kojima je dat značajan doprinos afirmaciji, razvoju i unapređenju privrede, odnosno privatnog preduzetništva u gradu Novom Pazaru.

Član 23

U jednoj godini može se dodeliti najviše 8 (osam) nagrada, u jednoj oblasti najviše 2 (dve).

Član 24

Odbor za obeležavanje praznika, za dodelu priznanja, saradnju i udruživanje sa drugim gradovima i opštinama, (u daljem tekstu: Odbor) najkasnije do 01. februara, u godini u kojoj se dodeljuje nagrada, objavljuje javni poziv za dostavljanje predloga za dodelu nagrade.

Javni poziv iz stava 1. ovog člana sa uslovima za dodeljivanje nagrade, objavljuje se u najmanje jednom lokalnom glasilu i najmanje jednom dnevnom glasilu koje izlazi na teritoriji Republike Srbije.

Predlozi za nagradu dostavljaju se odboru najkasnije do 01. marta u godini u kojoj se nagrada dodeljuje.

Član 25

Pravo predlaganja kandidata za nagradu imaju sva pravna i fizička lista.

Član 26

Predlog iz člana 24. Odluke dostavlja se u pisanoj formi sa obrazloženjem i sadrži podatke o autoru kandidatu i delu koje se predlaže, rezultate rada kandidata i naznaku oblasti za koju se kandidat predlaže.

Uz predlog se dostavlja i delo koje se predlaže ili odgovarajuća dokumentacija ako po prirodi stvari delo nije moguće dostaviti, koja se posle odlučivanja o nagradama ne vraća predlagaču.

Član 27

Lista kandidata predloženih u roku i na način utvrđen ovom Odlukom objavljuje se u lokalnim, odnosno dnevnim novinama iz člana 24. ove Odluke, najkasnije do 15. marta u godini u kojoj se nagrada dodeljuje.

Član 28

Gradonačelnik grada će po zahtevu Odbora obrazovati stručne komisije za ocenu dostavljenih predloga i davanje mišljenja Odboru o najboljem predlogu.

Stručna komisija ocenu i mišljenje dostavlja Odboru u vidu izveštaja.

Rad stručne komisije ja javan.

Stručna komisija donosi odluke većinom glasova od ukupnog broja članova.

Stručna komisija ima predsednika i najmanje četiri člana, koji se imenuju iz reda stručnjaka iz oblasti za koju se nagrada dodeljuje.

Predsednik, odnosno član komisije ne može biti lice koje je predloženo za nagradu grada Novog Pazara.

Predlog za dodelu nagrade utvrđuje Odbor i upućuje ga Skupštini grada, najkasnije do 1. aprila u godini u kojoj se nagrada dodeljuje.

Član 29

Odluku o dodeli GODIŠNJE NAGRADE GRADA NOVOG PAZARA, donosi Skupština grada Novog Pazara.

Član 30

Nagrada se uručuje u obliku Diplome i novčane nagrade.

Oblik i sadržina Diplome utvrđuje se na osnovu konkursa za idejno rešenje, koji raspisuje i o čijem se sprovođenju stara Služba gradske uprave nadležna za poslove kulture.

Diploma se uručuje u futroli, kraljevsko crvene boje, na kojoj je zlatotiskom ispisan tekst: Grad Novi Pazar, Gradonačelnik grada, svake godine utvrđuje visinu novčane nagrade u okviru sredstava utvrđenih budžetom grada za godinu u kojoj se nagrada dodeljuje.

Član 31

Nagradu uručuje gradonačelnik grada na svečanoj sednici Skupštine grada povodom praznika Dana grada.

V PLAKETA GRADA NOVOG PAZARA

Član 32

Plaketa grada Novog Pazara može se dodeliti domaćem ili stranom fizičkom, odnosno pravnom licu, kao i domaćim ili inostranim gradovima ili institucijama, za izuzetan doprinos u afirmaciji i promovisanju grada Novog Pazara, u zemlji ili inostranstvu kao i za pruženu pomoć gradu ili građanima Novog Pazara.

Član 33

Odluku o dodeljivanju Plakete grada Novog Pazara donosi gradonačelnik grada.

Plaketu grada Novog Pazara uručuje gradonačelnik grada.

Član 34

Plaketa grada Novog Pazara, uručuje se u obliku Plakete i Diplome.

Plaketa sadrži: na prednjoj strani (lice plakete) grb grada Novog Pazara u galvanizaciji pozlate, a na zadnjoj strani (naličje plakete) gravira se ime i prezime dobitnika i godina uručenja.

Plaketa se uručuje u kutiji, kraljevsko crvene boje.

Diploma je dimenzija B3, sa utisnutim grbom grada, izrađena od kvalitetnog grafičkog ili akvarel 250-300 gr papira, na kojoj je kaligrafskim slovima ispisan tekst:

Grad Novi Pazar

Dodeljuje

PLAKETU GRADA NOVOG PAZARA

__________________________________________________________
(ime i prezime)

za izuzetan doprinos afirmaciji i
promociji grada Novog Pazara
u zemlji i svetu

U Novom Pazaru,

 

GRADONAČELNIK GRADA
NOVOG PAZARA

 

 

U donjem levom uglu Diplome, na pečatnom vosku sa jemstvenikom, utisnut je pečat grba grada Novog Pazara.

U donjem desnom uglu Diplome, kaliografskim slovima ispisan je tekst: gradonačelnik grada Novog Pazara, ispod koga je gradonačelnik grada svojeručno potpisan.

Diploma se uručuje u futroli, kraljevsko crvene boje, na kojoj je zlatotiskom ispisan tekst: grad Novi Pazar.

Član 35

Ovom Odlukom ustanovljava se knjiga, u kojoj se pod rednim brojem, upisuju imena i prezimena, datumi rođenja, kao i ostali podaci o dobitnicima javnih priznanja, nagrada i zvanja počasnog građanina.

VI PRELAZNE I ZAVRŠNE ODREDBE

Član 36

Stupanjem na snagu ove Odluke, prestaje da važi Odluka o priznanjima opštine Novi Pazar ("Službeni list opštine Novi Pazar", br. 28/06).

Član 37

Ova odluka stupa na snagu osmog dana od dana objavljivanja u "Službenom listu grada Novog Pazara".

 

Samostalni član Odluke o izmeni
Odluke o ustanovljenju nagrada i javnih priznanja grada Novog Pazara

("Sl. list grada Novog Pazara", br. 4/2011)

Član 3

Ova Odluka stupa na snagu danom donošenja, a objaviće se u Službenom listu grada Novog Pazara.