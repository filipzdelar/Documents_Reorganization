Skupština Autonomne Pokrajine Vojvodine, kao osnivač studentskih centara sa sedištem na teritoriji Autonomne Pokrajine Vojvodine (u daljem tekstu: Pokrajina), ovom odlukom uređuje način rada studentskih centara sa sedištem na teritoriji Pokrajine, način korišćenja usluga studentskih centara i druga pitanja vezana za obavljanje delatnosti studentskih centara.

Član 2

Pravo na korišćenje usluga smeštaja i ishrane u studentskom centru sa sedištem na teritoriji Pokrajine imaju, saglasno zakonu kojim se uređuje oblast učeničkog i studentskog standarda, studenti državljani Republike Srbije.

Studenti, strani državljani, mogu koristiti usluge smeštaja i ishrane u studentskom centru, ako su stipendisti Republike Srbije na osnovu međudržavnih sporazuma.

Obavljanje delatnosti

Član 3

U obavljanju delatnosti utvrđene zakonom studentski centar:

- stvara studentima povoljne uslove za obrazovanje i razvija radne, kulturne i higijenske navike kod studenata,

- razvija kod studenata smisao za kolektivni život,

- izgrađuje kod studenata navike za kulturno-stvaralačko korišćenje slobodnog vremena, i sportsko-rekreativne aktivnosti,

- razvija kod studenata etničku i versku toleranciju i obezbeđuje ostvarivanje ustavom zagarantovanih ljudskih sloboda i prava,

- preduzima mere za jačanje poverenja i sprečava ponašanja koja narušavaju atmosferu međuetničke tolerancije i prava na razlikovanje,

- organizuje tribine sa programima u funkciji međusobnog upoznavanja, razvoja kreativnosti, takmičenja, edukacije za toleranciju i interkulturalnost, kulturno-zabavni život studenata i sportsko-rekreativne aktivnosti.

Razvojno-stvaralački programi studentskog centra

Član 4

Studentski centar ostvaruje sa studentima razvojno-stvaralačke programe koje utvrđuje prema potrebama i interesovanjima studenata.

Studentski centar donosi razvojno-stvaralačke programe u saradnji sa studentskom organizacijom, uz saglasnost pokrajinskog organa uprave nadležnog za poslove učeničkog i studentskog standarda.

Član 5

Studentski centar organizuje kulturno-zabavnu aktivnost studenata i sportsko-rekreativne aktivnosti studenata samostalno ili u saradnji sa drugim ustanovama i organizacijama.

Godišnji program rada

Član 6

Studentski centar donosi petogodišnji program razvoja delatnosti smeštaja i ishrane uz saglasnost Skupštine Autonomne Pokrajine Vojvodine.

Studentski centar donosi do 1. oktobra godišnji program rada za narednu školsku godinu.

Godišnji program rada i izveštaj o ostvarivanju ovog programa studentski centar dostavlja pokrajinskom organu uprave nadležnom za poslove učeničkog i studentskog standarda u roku od 30 dana od dana donošenja odnosno usvajanja.

Statusne promene i promene delatnosti

Član 7

Studentski centar može izvršiti statusne promene, promeniti naziv, sedište i delatnost samo uz prethodnu saglasnost Skupštine Autonomne Pokrajine Vojvodine.

Služba za razvojno-stvaralački program

Član 8

Studentski centar organizuje samostalno ili u saradnji sa srednjim školama i studentskim organizacijama službu za razvojno -stvaralačke programe.

Stručni saradnici

Član 9

Razvojno-stvaralački program sa studentima u studentskom centru može ostvarivati samo lice sa visokim obrazovanjem i položenim stručnim ispitom (u daljem tekstu: stručni saradnik).

Član 10

Stručni ispit polažu stručni saradnici na Filozofskom fakultetu u Novom Sadu po programu predviđenom za vaspitače u domovima učenika.

Član 11

Studentski centar donosi plan i program stručnog usavršavanja stručnih saradnika.

Stručno usavršavanje stručnih saradnika se ostvaruje u okviru 40-časovne radne sedmice.

Studentski centar je obavezan da vodi evidenciju učestvovanja stručnih saradnika u stručnom usavršavanju.

Član 12

Stručno usavršavanje stručnih saradnika obuhvata: produbljivanje stručnih znanja, pedagoških i psiholoških znanja i edukaciju za negovanje kod studenata odnosa tolerancije i multikulturalnosti.

Korišćenje usluga studentskog centra

Član 13

Studenti se primaju u studentski centar od 1. oktobra do 15. jula naredne godine.

Studentski centar zaključuje ugovor sa studentom koji je primljen u centar.

Ugovorom se utvrđuju međusobna prava i obaveze potpisnika ugovora u korišćenju usluga studentskog centra. Ugovor obavezno sadrži vreme korišćenja usluga studentskih centara.

Član 14

Student, koji za vreme letnjeg raspusta ima obaveze utvrđene nastavnim planom i programom više škole i fakulteta, ima pravo da koristi usluge studentskog centra, pod istim uslovima kao u vremenu od 1. oktobra do 15. jula, za vreme dok mu traju obaveze.

Studentski centar određuje objekat za smeštaj studenata iz stava 1. ovog člana.

Student koristi pravo iz stava 1. ovog člana na osnovu potvrde više škole i fakulteta.

Studentski centar zaključuje ugovor sa studentom koji koristi usluge po osnovu stava 1. ovog člana. Ugovor obavezno sadrži vreme korišćenja usluga studentskog centra.

Za korišćenje usluga studentskog centra za vreme letnjeg raspusta student je u obavezi da pre isteka roka korišćenja usluga utvrđenih u ugovoru dostavi potvrdu da ima obaveze po nastavnom planu i programu više škole ili fakulteta.

Član 15

U vreme septembarskog ispitnog roka, student ima pravo da koristi usluge studentskog centra u objektu koji odredi studentski centar pod uslovima iz člana 14. ove Odluke.

O korišćenju usluga studentskog centra iz stava 1. ovog člana zaključuje se poseban ugovor. Ugovor obavezno sadrži vreme korišćenja usluga studentskog centra.

Član 16

Studenti su dužni da koriste usluge studentskog centra na način utvrđen statutom i drugim opštim aktom centra.

Saglasnost na statut studentskog centra i opšti akt studentskog centra kojim se uređuje način korišćenja usluga centra daje Izvršno veće Autonomne Pokrajine Vojvodine.

Član 17

U korišćenju usluga studentskog centra studenti su dužni da se brižljivo odnose prema imovini studentskog centra.

Student koji učini materijalnu štetu u korišćenju usluga studentskog centra namerno ili iz krajnje nepažnje dužan je da je naknadi.

Direktor studentskog centra je dužan da pokrene postupak za utvrđivanje materijalne štete, visine štete, okolnosti pod kojima je šteta nastala, studenta koji je štetu prouzrokovao i način na koji će počinilac štete štetu nadoknaditi.

Član 18

Protiv studenta koji ne poštuje pravila ponašanja u studentskom centru pokreće se disciplinski postupak i izriču disciplinske mere: opomena, ukor, ukor pred zabranu korišćenja usluga studentskog centra, privremena i trajna zabrana korišćenja usluga studentskog centra.

Statutom studentskog centra utvrđuju se povrede discipline, organi i postupak izricanja i izvršavanja disciplinskih mera.

Ukor pred zabranu korišćenja usluga studentskog centra, privremena i trajna zabrana korišćenja usluga studentskog centra izriču se za teže povrede discipline.

Član 19

Težim povredama discipline smatraju se:

- ostvarivanje prava na korišćenje usluga studentskog centra na osnovu isprava sa lažnim podacima,

- falsifikovanje javnih isprava na osnovu kojih se ostvaruju prava na korišćenje usluga studentskog centra,

- prodaja prava na korišćenje usluga studentskih centara ili ustupanje ovog prava drugom licu,

- uništavanje imovine studentskog centra namerno ili iz krajnje nepažnje,

- raspirivanje rasne, nacionalne i verske mržnje i netrpeljivosti i radnje koje ukazuju na etničku i versku netoleranciju,

- organizovanje aktivnosti kojima se ugrožavaju ili omalovažavaju studenti korisnici usluga studentskog centra po osnovu rasne, nacionalne, jezičke, verske ili polne pripadnosti ili političkog opredeljenja ili podsticanje takvih aktivnosti,

- prodaja i konzumiranje u većim količinama alkohola i drugih psihoaktivnih supstanci,

- izazivanje tuče u studentskom centru,

- krađa imovine korisnika usluga studentskog centra ili imovine centra,

- odbijanje iseljavanja i posle isteka vremena na koje je primljen u studentski centar, i

- druge povrede predviđene statutom.

Za teže povrede discipline iz stava 1. alineje 1. do 10. ovog člana obavezno se izriče disciplinska mera trajne zabrane korišćenja usluga studentskog centra.

Ako je student ostvario pravo na korišćenje usluga studentskog centra na osnovu isprave sa lažnim podacima, koju je izdala viša škola ili fakultet, direktor studentskog centra je dužan da o tome odmah obavesti Pokrajinski sekretarijat za obrazovanje i kulturu.

Iseljenje iz studentskog centra

Član 20

Student je dužan da se iseli iz sobe koja mu je dodeljena na korišćenje najkasnije 15. jula.

Iseljenje sprovodi komisija koju obrazuje direktor studentskog centra. O primopredaji sobe sačinjava se zapisnik sa opisom stanja inventara u momentu primopredaje.

Član 21

Ako student posle isteka vremena na koje je primljen u studentski centar odbije da se iseli, direktor studentskog centra donosi rešenje o iseljenju.

Protiv rešenja o iseljenju koje je doneo direktor studentskog centra, student ima pravo prigovora pokrajinskom sekretaru nadležnom za poslove učeničkog i studentskog standarda, u roku od tri dana od dana dostavljanja rešenja.

Odluka pokrajinskog sekretara nadležnog za poslove učeničkog i studentskog standarda je konačna i donosi se po hitnom postupku.

Član 22

Konačna odluka o iseljenju iz studentskog centra je izvršna isprava.

Izvršenje dozvoljava i sprovodi opštinski odnosno gradski organ uprave nadležan za poslove stanovanja u sedištu studentskog centra.

Za iseljenje iz objekata studentskog centra van sedišta nadležan je opštinski organ uprave u mestu gde se nalazi organizaciona jedinica studentskog centra.

Član 23

Ako se lice useli u studentski centar bez odluke o dodeli mesta, direktor studentskog centra donosi rešenje o iseljenju iz centra.

Ako direktor studentskog centra ne donese rešenje o iseljenju, rešenje o iseljenju donosi pokrajinski sekretar nadležan za poslove učeničkog i studentskog standarda.

Protiv rešenja o iseljenju lice ima pravo žalbe pokrajinskom sekretaru nadležnom za poslove učeničkog i studentskog standarda u roku od tri dana od dana dostavljanja rešenja.

Žalba na rešenje ne zadržava izvršenje.

Izvršenje rešenja o iseljenju dozvoljava i sprovodi opštinski odnosno gradski organ uprave nadležan za poslove opšte uprave u sedištu studentskog centra odnosno organizacione jedinice studentskog centra.

KAZNENE ODREDBE

Član 24

Novčanom kaznom od 10.000,00 do 50.000,00 dinara kazniće se za prekršaj studentski centar:

- ako ne donese razvojno-stvaralačke programe (član 4. stav 2),

- ako ne donese godišnji program rada (član 6. stav 2),

- ako ne zaključi ugovor sa studentom koji je primljen u centar, (član 13. stav 2, član 14. stav 4. i član 15. stav 2),

- ako ne pokrene postupak za utvrđivanje materijalne štete (član 17. stav 3),

- ako ne pokrene postupak protiv studenta za teže povrede discipline (članovi 18. stav 1. i 19),

- ako za teže povrede discipline iz člana 19. ove Odluke ne izrekne disciplinsku meru trajne zabrane korišćenja usluga studentskog centra,

- ako ne pokrene postupak iseljenja iz studentskog centra (članovi 21. i 23).

Za prekršaj iz stava 1. ovog člana kazniće se i odgovorno lice u studentskom centru novčanom kaznom od 1.000,00 do 10.000,00 dinara.

Član 25

Ova odluka stupa na snagu osmog dana od dana objavljivanja u "Službenom listu Autonomne Pokrajine Vojvodine".