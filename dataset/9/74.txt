Ovom uredbom uređuju se bliži uslovi i način dodele priznanja u vidu doživotnog mesečnog novčanog primanja umetniku, odnosno stručnjaku u kulturi, za vrhunski doprinos nacionalnoj kulturi odnosno kulturi nacionalnih manjina (u daljem tekstu: Priznanje).

Priznanje predstavlja lično, neprenosivo pravo.

Član 2

Pravo na Priznanje može steći umetnik, odnosno stručnjak u kulturi za sledeća područja kulture:

1) književno stvaralaštvo;

2) književno prevodilaštvo;

3) muzičko stvaralaštvo;

4) muzička interpretacija;

5) likovna umetnost;

6) primenjena i vizuelna umetnost, dizajn i umetnička fotografija;

7) filmska umetnost i audio vizuelno stvaralaštvo;

8) pozorišna umetnost - scensko stvaralaštvo i interpretacija;

9) opera, muzičko scensko stvaralaštvo i interpretacija;

10) umetnička igra (klasičan balet, narodna igra i savremena igra);

11) digitalno stvaralaštvo i multimedija;

12) arhitektura;

13) prevođenje stručnih i naučnih tekstova;

14) izvođenje muzičkih, govornih, artističkih i scenskih kulturnih programa;

15) istraživanje, zaštita, korišćenje, prikupljanje i predstavljanje nepokretnog kulturnog nasleđa;

16) istraživanje, zaštita, korišćenje, prikupljanje i predstavljanje pokretnog kulturnog nasleđa;

17) istraživanje, zaštita, korišćenje, prikupljanje i predstavljanje nematerijalnog kulturnog nasleđa;

18) filmske i televizijske delatnosti;

19) naučnoistraživačka i edukativna delatnost u kulturi;

20) bibliotečko-informaciona delatnost;

21) izdavaštvo;

22) produkcija kulturnih programa i dela.

Član 3

Pravo na Priznanje može steći umetnik, odnosno stručnjak u kulturi, državljanin Republike Srbije, koji je dao vrhunski doprinos nacionalnoj kulturi, odnosno kulturi nacionalnih manjina, pod uslovom:

1) da je doprineo razvoju kulturnih vrednosti u Republici Srbiji odnosno njenoj međunarodnoj afirmaciji;

2) da je dobitnik najznačajnijih nagrada i priznanja, shodno listi koju utvrđuju reprezentativna udruženja, svako za svoje područje kulture, i to:

(1) republička nagrada za poseban doprinos razvoju kulture,

(2) nagrada za životno delo,

(3) stručne nagrade sa međunarodnih festivala,

(4) nagrade sa festivala i manifestacija od republičkog značaja,

(5) stručne nagrade udruženja,

(6) lokalne nagrade i društvena priznanja,

(7) druge stručne međunarodne i domaće nagrade i priznanja iz područja kulture;

3) da ima obrazloženu ocenu vrednosti doprinosa u oblasti kulture u područjima kulture iz člana 2. ove uredbe, koju daju umetnička i strukovna udruženja, ustanove kulture, te obrazovne i naučne ustanove.

Ovo priznanje dodeljuje se umetniku odnosno stručnjaku u kulturi koji je ostvario pravo na penziju.

Član 4

Ispunjenost uslova iz člana 3. ove uredbe utvrđuje Komisija.

Komisija ima sedam članova koji se imenuju iz reda uglednih i afirmisanih umetnika i stručnjaka u kulturi, od kojih je jedan predstavnik nacionalnih saveta nacionalnih manjina.

Članove Komisije imenuje ministar nadležan za poslove kulture na period od tri godine, sa mogućnošću ponovnog imenovanja.

Administrativno-tehničku potporu Komisiji pruža ministarstvo nadležno za poslove kulture (u daljem tekstu: Ministarstvo).

Član 5

Javni poziv za dostavljanje obrazloženog predloga za dodelu Priznanja oglašava se na zvaničnoj internet strani Ministarstva i u jednim dnevnim novinama koje se distribuiraju na celoj teritoriji Republike Srbije i traje 30 dana.

Član 6

Reprezentativna udruženja u kulturi podnose Ministarstvu obrazloženi predlog za dodelu Priznanja, posle održane zajedničke sednice za svako područje kulture iz člana 2. ove uredbe. Za svako područje kulture u toku godine reprezentativna udruženja u kulturi mogu da predlože najviše tri umetnika, odnosno stručnjaka u kulturi.

Nacionalni savet nacionalne manjine u toku jedne godine može da predloži jednog umetnika odnosno stručnjaka u kulturi, koji je pripadnik nacionalne manjine.

Uz obrazloženi predlog na javni poziv, pored dokaza o ispunjenosti uslova iz člana 3. ove uredbe, dostavlja se i:

1) kratka biografija predloženog kandidata;

2) obrazložena ocena vrednosti doprinosa u oblasti kulture;

3) lista nagrada i priznanja iz člana 3. ove uredbe.

Dokaze o ispunjenosti uslova kandidata za Priznanje prava iz člana 3. ove uredbe, u pogledu državljanstva i svojstva osiguranika u smislu propisa o penzijskom i invalidskom osiguranju, Ministarstvo pribavlja po službenoj dužnosti, uz saglasnost lica na koje se ti podaci odnose.

Član 7

Komisija razmatra dostavljene predloge iz člana 6. st. 1. i 2. ove uredbe, vrši uži izbor i, u roku do 60 dana od dana isteka javnog poziva, predlaže listu kandidata za dodelu Priznanja.

Član 8

Ministarstvo, polazeći od predloga Komisije i finansijskih sredstava obezbeđenih u budžetu Republike Srbije, predlaže Vladi akt o dodeli Priznanja, za najviše do 20 umetnika, odnosno stručnjaka kulture za svaku godinu u kojoj se oglašava javni poziv.

Vlada donosi rešenje o dodeli priznanja za vrhunski doprinos nacionalnoj kulturi, odnosno kulturi nacionalnih manjina.

Član 9

Priznanje se dodeljuje i isplaćuje umetniku, odnosno stručnjaku u kulturi u vidu doživotnog mesečnog novčanog primanja, u visini jedne prosečne zarade bez poreza i doprinosa u Republici Srbiji za mesec jul prethodne godine, prema podacima republičkog organa nadležnog za poslove statistike.

Priznanje se isplaćuje počev od prvog narednog meseca od meseca u kojem je doneto rešenje iz člana 8. stav 2. ove uredbe.

Priznanje se isplaćuje preko Ministarstva.

Član 10

Danom stupanja na snagu ove uredbe prestaje da važi Uredba o bližim uslovima i načinu dodele priznanja za vrhunski doprinos nacionalnoj kulturi, odnosno kulturi nacionalnih manjina ("Službeni glasnik RS", br. 36/10, 146/14 i 91/18).