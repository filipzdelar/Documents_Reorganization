1. Ovim uputstvom daju se instrukcije za izradu akta kojim ustanove obrazovanja i vaspitanja - predškolska ustanova, osnovna i srednja škola i škola sa domom učenika (u daljem tekstu: ustanova), propisuju mere, način i postupak zaštite i bezbednosti dece i učenika za vreme boravka u ustanovi i svih aktivnosti koje organizuje ustanova, u saradnji sa nadležnim organom jedinice lokalne samouprave i koje je dužna da sprovodi.

2. Ustanova donosi opšti akt o zaštiti i bezbednosti dece i učenika i da sprovodi mere propisane navedenim aktom kojima se osiguravaju uslovi za bezbedan boravak dece i učenika u ustanovi i tokom svih aktivnosti koje organizuje, u skladu sa Zakonom o osnovama sistema obrazovanja i vaspitanja ("Službeni glasnik RS", br. 88/17, 27/18 - dr. zakon, 10/19, 6/20 i 129/21), u daljem tekstu: Zakon, posebnim zakonima u oblasti obrazovanja i vaspitanja i drugim propisima kojima se uređuje oblast zaštite i bezbednosti.

Deca i učenici imaju pravo na zaštitu i bezbednost u objektu - zgradi i dvorištu ustanove, kao i van zgrade i dvorišta ustanove, za vreme ostvarivanja svih oblika vaspitno-obrazovnog, odnosno obrazovno-vaspitnog rada (u daljem tekstu: obrazovno-vaspitnog rada) ili drugih aktivnosti koje organizuje ustanova, u skladu sa zakonom i opštim aktom ustanove.

Ustanova, odnosno zaposleni u ustanovi obezbeđuju ostvarivanje prava deteta i učenika u skladu sa zakonom.

3. Deca i učenici imaju pravo na zaštitu i bezbednost u ustanovi i od postupaka drugih učenika, zaposlenih, roditelja, odnosno drugog zakonskog zastupnika deteta i učenika i trećih lica koji ugrožavaju njihovu bezbednost.

4. Deca i učenici imaju pravo na zaštitu i bezbednost od elementarnih nepogoda - poplava, zemljotresa, drugih nesreća/udesa, katastrofa ili drugih vanrednih okolnosti i situacija, koje mogu da ugroze bezbednost dece i učenika u ustanovi.

5. Imajući u vidu interes zaštite i bezbednosti dece i učenika u ustanovi i posebno zaštite od nasilja, izuzetno je važno da pre izmene i dopune postojećih, odnosno izrade novih akata, ustanova pripremi analizu potencijalnih i aktuelnih rizika u organizaciji rada ustanove (prostorni, tehnički, vremenski i drugi organizacioni uslovi) i to:

1) pristup objektu ustanove (zgradi i dvorištu) - pristupačnost dvorišta za kretanje svih učesnika u obrazovno-vaspitnom procesu; konfiguracija terena i blizina rizičnih saobraćajnica, mostova, pružnih prelaza, pešačkih prelaza, ležećih policajaca, semafora i dr.;

2) sigurnost prostora - ograđenost dvorišta i stepen rizika od mogućnosti pristupa trećih lica tom prostoru; osvetljenost prostora oko objekta; video nadzor; stanje pristupnih rampi i podiznih platformi; stanje gromobrana i instalacija - vodovodne, kanalizacione, električne i gasne mreže i dr.;

3) u unutrašnjem prostoru - stanje objekata u kojima se ostvaruje obrazovno-vaspitni rad; pristupačnost samog objekta za sve učesnike u obrazovno-vaspitnom procesu; obezbeđeni pristup uređajima za grejanje i prostoru za skladištenje ogreva, uređajima za obezbeđivanje dovoda električne energije i sanitarnim čvorovima i njihovo stanje; stanje pristupnih rampi, podiznih platformi, lifta, stepeništa i sigurnosti rukohvata; stanje fiskulturne sale, opreme i sportskih terena; stanje radionica i kabineta sa mašinama, aparatima i drugim uređajima, alatima, priborom, hemikalijama i drugim sredstvima za rad (posebno u određenim stručnim školama) i dr.;

4) specifični uslovi i okolnosti koje su karakteristične za ustanovu - rad u smenama, broj dece i učenika, gradska ili seoska sredina i dr.

6. Radi sveobuhvatne zaštite i bezbednosti dece i učenika, ustanova u saradnji sa nadležnim organom jedinice lokalne samouprave, ostvaruje komunikaciju sa relevantnim institucijama na nacionalnom i lokalnom nivou (organi, organizacije, ustanove, tela i dr.) u realizaciji zajedničkih aktivnosti usmerenih na obezbeđivanje zaštite i bezbednosti dece i učenika.

7. Ustanova utvrđuje preventivne mere zaštite i bezbednosti u vezi sa organizacijom rada, i to:

1) raspored dežurstava zaposlenih;

2) način evidentiranja ulaska trećih lica u ustanovu;

3) mogućnosti boravka u ustanovi, odnosno neposrednog učešća roditelja, odnosno drugog zakonskog zastupnika deteta i učenika u aktivnostima ustanove;

4) način utvrđivanja identiteta lica koja ostvaruju roditeljsko pravo ili imaju starateljstvo nad detetom kada ga odvode iz ustanove;

5) fizička bezbednost objekta - zgrade, dvorišta i okruženja (procedure za domara/zaposlene - svakodnevni obilasci zgrade (učionica, hodnika, toaleta, radionica, sportske sale, trpezarije i drugih prostorija) i dvorišta, sa posebnim osvrtom na igrališta za decu, terene i sportske sprave koje koriste i građani; periodične provere mašina, alata, prostora; dezinfekcija, dezinsekcija i deratizacija; provera ispravnosti vode za piće nakon nekih havarija i klima uređaja - redovno održavanje i ventilacija; saobraćajna bezbednost i sl.);

6) bezbednosne procedure u ustanovi, uključujući i procedure u učionicama i drugim prostorijama za rad - opremanje prostorija, biljke koje izazivaju alergije ili otrovne biljke, upotreba sprejova ili toksičnih lepkova, otvaranje prozora i dr.; u laboratorijama - protokoli izvođenja ogleda, zaštitna oprema i procedure; čuvanje hemikalija i opasnih alata; u fiskulturnoj sali - rad na spravama; bezbednost u radnim prostorima - obratiti pažnju na specifičnosti ustanova: elektrotehničke škole, građevinske, mašinske, medicinske i dr. i posebno na izvođenje nastave na otvorenom kada je pretoplo ili suviše hladno; u dvorištu - održavanje bezbednosti kretanja u dvorištu kada napada sneg i dr., bezbednost saobraćaja u dvorištu ustanove (zabrana kretanja motornih vozila i vozila mikromobilnosti kroz dvorište, zabrana ulaska osim za vozila ustanove);

7) održavanje discipline u ustanovi - zgradi i njenom dvorištu, posebno u učionici i drugim radnim prostorijama;

8) istaknuto mesto za prvu pomoć u ustanovi (gde se nalazi komplet za prvu pomoć, ko je zadužen da proverava/dopunjuje sadržinu kompleta, telefoni hitne pomoći i nadležnog doma zdravlja - vidno obeleženi) kao i način postupanja u situaciji kada je potrebno detetu i učeniku ukazati prvu pomoć ili postoji sumnja na potencijalni zdravstveni rizik ili povredu deteta, odnosno učenika (ko poziva hitnu pomoć i obaveštava roditelja i drugog zakonskog zastupnika i po potrebi nadležnu inspekciju i dr.);

9) postupanje radi zaštite od bolesti, posebno infektivnih (higijena u ustanovi, postupanje u skladu sa propisima iz oblasti zdravstva i sanitarna kontrola, ko je zadužen da obavesti nadležnog lekara o pojavi infektivne bolesti i dr.) i postupanje po preporukama nadležnih zdravstvenih organa;

10) bezbednosne procedure/pravila za zaštitu i bezbednost za vreme ostvarivanja obrazovno-vaspitnog rada van ustanove (praktična nastava i/ili učenje kroz rad u skladu sa planom i programom nastave i učenja, nastava u prirodi, ekskurzije, studijska putovanja i takmičenja - voditi računa o tome da se u cilju zaštite i bezbednosti prevoz učenika za republičko takmičenje, po pravilu, ne vrši u kasnim noćnim ili ranim jutarnjim satima da bi učestvovali na takmičenju), odlasci u pozorišta, posete muzejima i realizacija drugih oblika vanškolskih aktivnosti;

11) postupanje radi zaštite od fizičkih povreda (obezbediti da podovi nisu klizavi ili staviti odgovarajuću oznaku; obezbediti nabavku školskog nameštaja bez oštrih ivica, u skladu sa mogućnostima i dr.);

12) način pravilnog korišćenja i nadzor nad upotrebom mašina, aparata i drugih uređaja u realizaciji nastave, kao i alata, hemikalija i drugih nastavnih sredstava; redovna provera ispravnosti mašina, aparata i drugih uređaja; upotreba propisane zaštitne opreme i dr.;

13) pravila za odgovorno postupanje i prijava kvarova i oštećenja na instalacijama - vodovodne, kanalizacione, električne i gasne mreže, kotlarnice i dr.; odrediti ko je zadužen da reaguje radi zaštite i bezbednosti ili da prijavi kvar na instalacijama, opremi i dr. nadležnoj službi;

14) planiranje preventivnih i interventnih programa sa ciljem promene ponašanja kod učenika;

15) saradnja sa ovlašćenim organizacijama za kontrolu gromobranskih instalacija, u skladu sa zakonom;

16) mere zaštite od požara, u skladu sa Zakonom o zaštiti od požara ("Službeni glasnik RS ", br. 111/09, 20/15 i 87/18 - dr. zakon);

17) pravila za odgovorno postupanje u slučaju elementarnih nepogoda i drugih nesreća i sl. ili drugih vanrednih okolnosti i situacija;

18) pravila za saradnju sa komunalnim službama radi obezbeđenja trotoara i uličnog osvetljenja na prilazu ustanovi, pravilnog razmeštaja/postavljanja kontejnera, da ne ometaju ulaz u dvorište ustanove i zgradu i dr.;

19) pravila za saradnju sa nadležnim službama radi postavljanja odgovarajuće zaštitne signalizacije na saobraćajnicama na prilazu ustanovi;

20) izradu i realizaciju godišnjih programa zaštite od nasilja, zlostavljanja i zanemarivanja i programa sprečavanja diskriminacije u skladu sa Zakonom, Pravilnikom o protokolu postupanja u ustanovi u odgovoru na nasilje, zlostavljanje i zanemarivanje ("Službeni glasnik RS", br. 46/19 i 104/20) i Pravilnikom o postupanju ustanove u slučaju sumnje ili utvrđenog diskriminatornog ponašanja i vređanja ugleda, časti ili dostojanstva ličnosti ("Službeni glasnik RS", broj 65/18);

21) pravila za postupanje u slučaju pretnje, odnosno sumnje na postojanje druge opasnosti po bezbednost dece i učenika.

8. U propisivanju mera, načina i postupaka zaštite i bezbednosti dece i učenika za vreme boravka u ustanovi i svih aktivnosti koje organizuje, ustanova sarađuje sa nadležnim organom jedinice lokalne samouprave (član 108. stav 1. i član 189. stav 1. tačka 8) Zakona).

U postupku propisivanja mera za zaštitu i bezbednost dece i učenika učestvuje i savet roditelja u ustanovi (član 120. stav 6. tačka 9) Zakona), a u školi se pribavlja i mišljenje učeničkog parlamenta (član 88. stav 1. tačka 1) Zakona).

Mere, način i postupak iz stava 1. ove tačke, ustanova propisuje u skladu sa ovim uputstvom i odgovarajućim pravilima i merama, odnosno protokolima za postupanje nadležnih službi.

9. Ustanova o merama zaštite i bezbednosti koje donese u skladu sa ovim uputstvom, na odgovarajući način (usmena informacija, pismeno obaveštenje, na oglasnoj tabli ili zvaničnoj internet stranici ustanove i sl.) upoznaje decu i učenike, njihove roditelje, odnosno drugog zakonskog zastupnika, kao i sve zaposlene.

10. Ovo uputstvo stupa na snagu osmog dana od dana objavljivanja u "Službenom glasniku Republike Srbije".