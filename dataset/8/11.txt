UREDBA
O NAČINU I POSTUPKU PRIJAVLJIVANJA DRŽAVNE POMOĆI
("Sl. glasnik RS", br. 13/2010)

Član 1

Ovom uredbom se bliže propisuje način i postupak prijavljivanja šeme državne pomoći i individualne državne pomoći.

Član 2

Državna pomoć se prijavljuje kao šema državne pomoći i kao individualna državna pomoć u skladu sa članom 12. Zakona o kontroli državne pomoći.

Član 3

Davalac državne pomoći, odnosno predlagač propisa koji predstavlja osnov za dodelu državne pomoći (u daljem tekstu: podnosilac prijave), podnosi prijavu državne pomoći pre dodele, odnosno pre upućivanja propisa u proceduru donošenja, Komisiji za kontrolu državne pomoći (u daljem tekstu: Komisija).

Uz prijavu državne pomoći podnosi se Opšti obrazac državne pomoći, a po potrebi i Posebni obrazac za prijavljivanje državne pomoći.

Na zahtev Komisije, podnosilac prijave dostavlja i druge podatke i informacije, u skladu sa članom 2. stav 1. tačka 4) Zakona o kontroli državne pomoći.

Član 4

Opšti obrazac prijave državne pomoći iz člana 3. stav 2. ove uredbe, i Uputstvo za popunjavanje, odštampani su uz ovu uredbu i čine njen sastavni deo.

Poseban obrazac za prijavljivanje državne pomoći objavljuje se na internet prezentaciji Komisije.

Član 5

Prijava državne pomoći, podnosi se Komisiji preko Ministarstva finansija, poštom i u elektronskom obliku.

Član 6

Ova uredba stupa na snagu osmog dana od dana objavljivanja u "Službenom glasniku Republike Srbije".

 

Prilog 1.

OPŠTI OBRAZAC PRIJAVE DRŽAVNE POMOĆI

 

OPŠTI PODACI

Status prijave državne pomoći

Da li se podaci dostavljeni u ovom obrascu odnose na:

c Prijavu za dodelu državne pomoći na osnovu Uredbe o pravilima za dodelu državne pomoći ("Službeni glasnik RS", broj 13/10)

c Pomoć koja se ne smatra državnom pomoći, i koja se prijavljuje Komisiji za kontrolu državne pomoći, iz razloga pravne sigurnosti.

Navesti razloge, zašto se smatra da pomoć ne predstavlja državnu pomoć:

c Nema prenosa javnih sredstava

c Nema ekonomske prednosti za korisnike državne pomoći

c Nema selektivnosti

c Nema narušavanja konkurencije

Priložiti odgovarajuće dokaze.

1. Podaci o podnosiocu prijave

1.1. Naziv, sedište i matični broj:

___________________________________________________________________________________________________________

1.2. Kontakt osoba:

___________________________________________________________________________________________________________

Ime i prezime:

___________________________________________________________________________________________________________

Telefon/faks:

___________________________________________________________________________________________________________

E-mail:

___________________________________________________________________________________________________________

2. Podaci o državnoj pomoći koja se prijavljuje

2.1. Naziv državne pomoći:



2.2. Način/oblik prijavljivanja državne pomoći:

c Šema državne pomoći

c Individualna državna pomoć

2.3. Pravni osnov za dodelu državne pomoći:

Naziv osnovnog propisa:



Poziv na druge propise:



2.4. Da li se radi o novoj državnoj pomoći?

c Da

c Ne, pomoć zamenjuje/dopunjava već postojeću

Broj rešenja Komisije: _____________________________________________________________________

Datum odobrenja: ________________________________________________________________________

Naziv pomoći: ____________________________________________________________________________

2.5. Nivo sa kojeg se dodeljuje državna pomoć:

c Republika

c Autonomna pokrajina

c Jedinica lokalne samouprave

c Drugo

2.6. Ciljevi dodele državne pomoći:

 

Primarni cilj
(označiti samo jedan cilj)

Sekundarni cilj
(označiti najviše dva cilja)

1. Regionalna državna pomoć

c

c

 

2. Horizontalna državna pomoć

c

c

2.1. Mali i srednji privredni subjekti

c

c

2.2. Sanacija

c

c

2.3. Restrukturiranje

c

c

2.4. Zapošljavanje

c

c

2.5. Zaštita životne sredine

c

c

2.6. Istraživanje i razvoj

c

c

2.7. Usavršavanje

c

c

2.8. Rizični kapital

c

c

2.9. Kultura

c

c

 

3. Sektorska državna pomoć

c

c

 

3.1. Čelik

c

c

3.2. Ugalj

c

c

3.3. Saobraćaj

c

c

 

4. Državna pomoć male vrednosti - de minimis pomoć

c

c

 

5. Ostala državna pomoć

c

c

2.7. Vrsta i opis opravdanih troškova koji služe kao osnovica za dodelu državne pomoći:

___________________________________________________________________________________________________________

___________________________________________________________________________________________________________

2.8. Planirano trajanje državne pomoći:

Od: _______________________________________________

Do: _______________________________________________

2.9. Planirani iznos državne pomoći:

___________________________________________________________________________________________________________

2.10. Visina (intenzitet) pomoći:

___________________________________________________________________________________________________________

3. Podaci o korisnicima državne pomoći

3.1. Ako se državna pomoć prijavljuje kao šema državne pomoći:

3.1.1. Vrsta korisnika:

c svi privredni subjekti

c veliki privredni subjekti

c srednji privredni subjekti

c mali privredni subjekti

c ostali korisnici:

3.1.2. Procenjeni broj korisnika državne pomoći:

c manje od 10

c od 11 do 50

c od 51 do 100

c od 101 do 500

c od 501 do 1.000

c više od 1.000

3.2. Ako se državna pomoć prijavljuje kao individualna državna pomoć:

3.2.1. Naziv, sedište i matični broj korisnika državne pomoći:

___________________________________________________________________________________________________________

3.2.2. Vrsta privrednog subjekta - korisnika državne pomoći:

c Mali privredni subjekat

c Srednji privredni subjekat

c Veliki privredni subjekat

c Privredni subjekat u teškoćama

3.3. Sektor (delatnost) korisnika:

___________________________________________________________________________________________________________

4. Instrumenti i izvori finansiranja državne pomoći

4.1. Instrumenti:

c Subvencije

c Subvencionisanje kamata

c Krediti pod povoljnijim uslovima (sa podacima o uslovima dodele)

Rok vraćanja kredita:

___________________________________________________________________________________________________________
Period počeka:

___________________________________________________________________________________________________________
Kamatna stopa:

___________________________________________________________________________________________________________
Način osiguranja vraćanja kredita:

___________________________________________________________________________________________________________

c Poreski podsticaji

c Poreske olakšice

c Poreski kredit

c Otpis poreza

c Oslobađanje od plaćanja poreza

c Drugo

 

c Smanjenje doprinosa za obavezno socijalno osiguranje

c Kapitalna ulaganja/Ulaganje u rizični kapital

c Otpis duga

 

c Garancije (uključujući i podatke o kreditu ili drugim finansijskim transakcijama koje su obuhvaćene garancijama, podatke o osiguranju i isplati premija)

c Drugo

___________________________________________________________________________________________________________

4. 2. Izvori finansiranja:

c Budžet (Republike, AP, jedinice lokalne samouprave)

___________________________________________________________________________________________________________

c Ostali izvori:

___________________________________________________________________________________________________________

5. Usklađenost prijave sa Uredbom o pravilima za dodelu državne pomoći

Navesti odeljak i članove Uredbe o pravilima za dodelu državne pomoći sa kojima je usklađena državna pomoć koja se prijavljuje:

___________________________________________________________________________________________________________

___________________________________________________________________________________________________________

Navesti koji se dopunski obrazac prilaže uz ovu opštu prijavu državne pomoći:

___________________________________________________________________________________________________________

___________________________________________________________________________________________________________

c Regionalna državna pomoć

c Pomoć za male i srednje privredne subjekte

c Pomoć za sanaciju

c Pomoć za restrukturisanje

c Pomoć za zapošljavanje

c Pomoć za zaštitu životne sredine

c Pomoć za istraživanje, razvoj i inovacije

c Pomoć za usavršavanje

c Pomoć za rizični kapital

c Pomoć za kulturu

c Pomoć sektoru saobraćaja

6. Kumulacija različitih vrsta državne pomoći

Da li se pomoć može kumulirati sa pomoćima dobijenim na osnovu šema sa drugih lokalnih, regionalnih ili državnih nivoa dodele pomoći, za pokriće istih opravdanih troškova?

c Da

 

c Ne

Ukoliko je odgovor DA, navesti mehanizme kojima se obezbeđuje poštovanje pravila o kumulaciji pomoći:

7. Poslovna tajna

Da li prijava sadrži poverljive informacije koje ne treba da budu dostupne trećim stranama?

c Da

 

c Ne

Ako je DA, navesti koji su delovi prijave poverljivi:

___________________________________________________________________________________________________________
___________________________________________________________________________________________________________
___________________________________________________________________________________________________________

8. Druge informacije

Navesti sve dodatne informacije za koje se smatra da su merodavne za dodelu pomoći:

___________________________________________________________________________________________________________
___________________________________________________________________________________________________________
___________________________________________________________________________________________________________
___________________________________________________________________________________________________________

9. Prilozi

Navesti svu dokumentaciju koja je priložena uz prijave, i priložite kopije u pisanom ili elektronskom obliku, ili dostavite internet adrese koje se odnose na predmetne dokumente.

10. Izjava

Potvrđujem da su podaci navedeni u ovom obrascu, i njegovi prilozi tačni i potpuni.

Mesto i datum:

___________________________________________________________________________________________________________
Potpis:

___________________________________________________________________________________________________________
Ime i prezime odgovorne osobe:

___________________________________________________________________________________________________________

 

UPUTSTVO
ZA POPUNJAVANJE OPŠTEG OBRASCA PRIJAVE DRŽAVNE POMOĆI

Opšti podaci

Status prijave državne pomoći - naznačiti da li se radi o:

- prijavi za dodelu državne pomoći na osnovu Uredbe o pravilima za dodelu državne pomoći ("Službeni glasnik RS", broj 13/10) ili

- dodeli sredstava korisnicima iz člana 2. tačka 3) Zakona o kontroli državne pomoći, a da istovremeno nisu kumulativno ispunjena sva četiri navedena uslova, prijavljuje se Komisiji iz razloga pravne sigurnosti, uz navođenje uslova koji nije ispunjen.

1. Podaci o podnosiocu prijave

Podnosilac prijave je davalac državne pomoći odnosno predlagač propisa koji predstavlja osnov za dodelu državne pomoći (ministarstvo, Fond za razvoj i dr.).

Osoba za kontakt - osoba koja je ovlašćena za davanje informacija vezanih za prijavu.

2. Podaci o državnoj pomoći koja se prijavljuje

2.1. Naziv državne pomoći:

Upisati naziv državne pomoći pod kojim će se državna pomoć evidentirati, a koja u nazivu sadrži primarni cilj dodele državne pomoći.

2.2. Način/oblik prijavljivanja državne pomoći

2.2.1. Šema - utvrđuje se po programima i propisima za unapred neodređene korisnike pomoći

2.2.2. Individualna pomoć - utvrđuje se po predlogu odluke o odobravanju odnosno dodeli pomoći pojedinačno navedenom - poznatom korisniku/korisnicima.

2.3. Pravni osnov za dodelu državne pomoći

Naziv osnovnog propisa - osnovni propis koji predstavlja neposredni osnov za dodelu državne pomoći (zakon uredba, pravilnik, program, odluka i dr.).

Drugi propisi - su propisi koji su po mišljenju podnosioca prijave relevantni za ocenu dozvoljenosti prijavljene državne pomoći.

2.4. Da li se radi o novoj državnoj pomoći?

Označiti odgovarajući odgovor:

Ako se radi o dopuni ili zameni postojeće pomoći (pomoć koja je bila odobrena) navesti tražene podatke.

2.5. Nivo dodele pomoći - označiti odgovarajući nivo.

Za opciju drugo - navesti pravno lice koje je u većinskom ili upravljačkom vlasništvu države (Fond za razvoj, AOFI, SIEPA).

2.6. Ciljevi dodele pomoći

Označiti jedan primarni cilj i najviše dva sekundarna, na primer primarni cilj može biti regionalni razvoj, a sekundarni razvoj malih i srednjih privrednih subjekata, i/ili zapošljavanje.

2.7. Vrsta i opis opravdanih troškova koji služe kao osnovica za dodelu državne pomoći

Navesti vrstu i opis opravdanih troškova koji služe kao osnovica za dodelu pomoći; na primer investiciona ulaganja, troškovi zarada za novootvorena radna mesta.

2.8. Trajanje pomoći - navesti planirani vremenski period trajanja pomoći.

2.9. Planirani iznos pomoći

Navesti ukupan planirani iznos pomoći za period trajanja pomoći.

2.10. Visina državne pomoći (intenzitet) predstavlja dozvoljeni procenat pomoći koji se izračunava na osnovu opravdanih troškova. Ako se radi o de minimis pomoći navodi se apsolutni iznos pomoći po korisniku.

3. Podaci o korisnicima državne pomoći

3.1. Ako se prijavljuje šema državne pomoći:

3.1.1. Označiti vrstu korisnika, a razvrstavanje pravnih subjekata uraditi prema članu 7) Zakona o računovodstvu i reviziji ("Službeni glasnik RS", broj 46/06).

3.2. Ako se prijavljuje individualna državna pomoć, popuniti tražene podatke za korisnika državne pomoći, a razvrstavanje privrednih subjekata kao pod tačkom 1.

Privredni subjekat u teškoćama, definisan je u članu 2. Uredbe o pravilima za dodelu državne pomoći.

3.3. Sektori - delatnosti korisnika, navesti prema Zakonu o Klasifikaciji delatnosti i Registru jedinica razvrstavanja ("Službeni list SRJ", br. 31/96, 12/98, 59/98 i 74/99), do nivoa grane (trocifrena oznaka). Kod šema državne pomoći, kada se radi o većem broju unapred nepoznatih korisnika, i ako nije određen sektor kojem je namenjena pomoć, navesti sektor koji je isključen iz dodele pomoći.

4. Instrumenti i izvori finansiranja državne pomoći

4.1. Navesti odgovarajući instrument dodele pomoći. Ako se radi o kreditima koji su odobravani pod različitim uslovima, upisati kod pojedinih uslova, na primer kamatne stope ili rok vraćanja kredita: "od - do".

Drugi poreski podsticaji, mogu da budu oslobađanja ili olakšice, odnosno umanjeno ostvarenje neporeskih prihoda (naknade, takse).

4.2. Izvori finansiranja, ako se radi o budžetskim rashodima navesti ekonomsku klasifikaciju, do nivoa subanalitičkog konta. Ako je instrument dodele državne pomoći umanjeno ostvarenje javnih prihoda, na primer kod poreskih podsticaja (član 28. tačka 8) Zakona o budžetskom sistemu, "Službeni glasnik RS", broj 51/09), navesti odgovarajuću ekonomsku klasifikaciju, u okviru kontnog plana - klasa prihoda u budžetu (7111, 7112, 7141), na koju se odnosi umanjeno ostvarenje prihoda.

Drugi izvori finansiranja su sredstva drugih pravnih lica, koja su po osnivačkom kapitalu i/ili po upravljanju u većinskom vlasništvu Republike, Autonomne pokrajine, ili jedinica lokalne samouprave, na primer Fond za razvoj, AOFI, SIEPA, javna preduzeća.

5. Navesti odgovarajuće odeljke i članove Uredbe o kontroli državne pomoći, i koji se dopunski obrazac prilaže uz prijavu.

6. Kumulacija pomoći - kumulacija (zbrajanje) državne pomoći se odnosi na iste opravdane troškove ukoliko se jednom privrednom subjektu dodeljuje pomoć po osnovu više šema državnih pomoći ili u kombinaciji šeme i individualne državne pomoći, ili se pomoć dodeljuje sa različitih nivoa vlasti (Republika, AP, jedinica lokalne samouprave), pri čemu se primenjuje samo jedan i to najpovoljniji intenzitet državne pomoći utvrđen Uredbom. Obaveza davaoca državne pomoći je, da pre dodele korisniku državne pomoći, obezbedi izjavu korisnika da li je i po kom osnovu već primio državnu pomoć po osnovu istih opravdanih troškova.

Za tačke 7, 8, 9. i 10. navesti tražene podatke.