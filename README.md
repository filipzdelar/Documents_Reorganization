## Installing liberies

```
pip install pickle
pip install re
pip install emoji
pip install nltk
pip install sklearn
pip install gensim
pip install numpy
pip install googletrans
pip install tkinter
pip install scipy
pip install turtle
```

## Preprocssing

```
python preprocess.py
python tokenizer.py
```

## Tokenizer

```
python tf-idf.py
```

or at notebook
tf-idf.ipynb


## Demo app

cd demo
python app.py 

or create exe file
```
pip install pyinstaller
pyinstaller -–onefile -–windowed --icon="images\myicon.ico" app.py

```

And run .exe file