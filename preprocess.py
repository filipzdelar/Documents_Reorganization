from googletrans import Translator
import pickle

sentance = []

for file in range(6):
	with open('./dataset/1/' + str(file+1) + '.txt', 'r', encoding='utf8') as f:
		lines = f.readlines()
		for line in lines:
			if len(line) > 2:
				if line[-2] == '.' or line[-1] == '\n' or line[-2] == ';':
					sentance.append(line)	


for file in range(52):
	with open('./dataset/8/' + str(file+1) + '.txt', 'r', encoding='utf8') as f:
		lines = f.readlines()
		for line in lines:
			if len(line) > 2:
				if line[-2] == '.' or line[-1] == '\n' or line[-2] == ';':
					sentance.append(line)	

					
for file in range(74):
	with open('./dataset/9/' + str(file+1) + '.txt', 'r', encoding='utf8') as f:
		lines = f.readlines()
		for line in lines:
			if len(line) > 2:
				if line[-2] == '.' or line[-1] == '\n' or line[-2] == ';':
					sentance.append(line)	

translator = Translator()

sentance_en = []
print(len(sentance))
counter = 0

for sentence_sr	in sentance:
	try:
		result = translator.translate(sentence_sr, src='sr', dest='en')
		sentance_en.append(result.text)
		counter += 1
		if counter == 100: 
			print(result.text)
			print(counter)
			counter = 0
	except:
		print("Error")

print(len(sentance_en))
pickle.dump(sentance_en, open("corpus_eng.p", "wb"))
