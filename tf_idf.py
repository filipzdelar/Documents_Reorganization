import pickle
import gensim
from gensim.models import CoherenceModel, LdaModel, TfidfModel
from sklearn.decomposition import NMF
from gensim.models.nmf import Nmf
import numpy as np


corpus_eng_processed = []
corpus_eng_processed = pickle.load(open(".//corpus_eng_processed.p", "rb"))

print(len(corpus_eng_processed))

dictionary = gensim.corpora.Dictionary(corpus_eng_processed)

dictionary.filter_extremes(
    no_below=2,
    no_above=0.9,
    keep_n=100000
)

corpus = [dictionary.doc2bow(text) for text in corpus_eng_processed]

tfidf = list(TfidfModel(dictionary=dictionary)[corpus])

topic_nums = list(np.arange(13, 15, 1))

coherence_scores_t = []
coherence_scores_b = []

for num in topic_nums:
    print(num)    
    nmf3 = Nmf(corpus=tfidf,
              num_topics=num,
              id2word=dictionary,
              chunksize=500,
              passes=10,
              kappa=.1,
              minimum_probability=0.01,
              w_max_iter=300,
              w_stop_condition=0.0001,
              h_max_iter=100,
              h_stop_condition=0.001,
              eval_every=10,
              normalize=True,
              random_state=42
    )

    cm3 = CoherenceModel(
        model=nmf3,
        texts=corpus_eng_processed,
        dictionary=dictionary,
        coherence='c_v'
    )
    
    coherence_scores_t.append(round(cm3.get_coherence(), 5))

scores_t = list(zip(topic_nums, coherence_scores_t))

print(scores_t)

max = scores_t[0]
for score in scores_t:
  if max[1] < score[1]:
    max = score

print(max)

nmf = Nmf(corpus=tfidf,
          num_topics=max[0],
          id2word=dictionary,
          chunksize=500,
          passes=10,
          kappa=.1,
          minimum_probability=0.01,
          w_max_iter=300,
          w_stop_condition=0.0001,
          h_max_iter=100,
          h_stop_condition=0.001,
          eval_every=10,
          normalize=True,
          random_state=42
)

nmf.save("nmf_save_tmp")

topics = nmf[TfidfModel(dictionary=dictionary)[corpus]]

for t in range(14):
  print()
  print('Topic '+ str(t) + ":")
  for to, score_of_each_topic in nmf.show_topic(t):
    print("\t"+to)


topics_best_score = [sorted(t, key=lambda t: t[1]) for t in topics]

topics_for_docs = [tr[-1][0] if (len(tr) != 0) else None for tr in topics_best_score]
print(topics_for_docs)

map_topics = {
  0 : "Javne usluge",
  1 : "Državne institucije",
  2 : "Investicije",  
  3 : "Umetnost i kultura",
  4 : "Pravne regulative",  
  5 : "Uprava", 
  6 : "Prekršaji",
  7 : "Tržište",
  8 : "Finansijske",
  9 : "Edukacione",
  10 : "Nadležnost",
  11 : "Vremenska odrednica",
  12 : "Nesvrstano",
  13 : "Pokrainske nadleženosti"
}

topics_total_amount = []

for iter in range(14):
  topics_total_amount.append(0)

for t in range(len(topics_for_docs)):
  if topics_for_docs[t] != None:
    topics_total_amount[topics_for_docs[t]] += 1

topics_total_amount


from matplotlib import colors
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib.colors import Normalize
from numpy.random import rand
import matplotlib.patches as mpatches


plt.rcParams["figure.figsize"] = [11.50, 8.50]
plt.rcParams["figure.autolayout"] = True

colours = ['black','brown','darkred','red','orange','yellow', 'yellowgreen','green','blue','darkblue','slateblue','purple','pink','grey']

space = " "
for i in range(14):
  plt.bar(space, topics_total_amount[i], color=colours[i], label=map_topics[i])
  space += " "

#plt.bar([x for x in range(14)], topics_total_amount)

import matplotlib.patches as mpatches

patchs =[]

for i in range(14):
  patchs.append(mpatches.Patch(color=colours[i], label=map_topics[i]))

#plt.xticks([0,1,2,3,4,5,6,7,8,9,10,11,12,13])

#plt.xticks([0,1,2,3,4,5,6,7,8,9,10,11,12], ['Februar', 'Mart', 'April', 'Maj', 'Jun', 'Jul', 'Avgust','Septembar', 'Oktobar', 'Novembar', 'Decembar', 'Januar', 'Februar'])
#plt.set_xticklabels([topic for topic in map_topics], rotation='vertical', fontsize=18)

plt.legend(handles=patchs, loc='upper left', prop={'size': 15})

plt.show()